import * as actionTypes from '../actions/actionTypes'

const initialState = {
    userList: [],
    loading: false,
    error: null,
    totalPages: null,
    totalElements: null,
    pageSize: null,
    pageNumber: 0
};

const userListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USERS_SUCCESS:
            return {
                ...state, userList: action.userList, totalPages: action.totalPages,
                totalElements: action.totalElements, pageSize: action.pageSize,
                pageNumber: action.pageNumber, loading: false, error: null
            };
        case actionTypes.FETCH_USERS_FAIL:
            return {...state, loading: false, error: action.error};
        case actionTypes.FETCH_USERS_START:
            return {...state, loading: true};
        case actionTypes.FETCH_USERS_INIT:
            return {...state, error: null, userList: null};
        default:
            return state;
    }
};

export default userListReducer;
