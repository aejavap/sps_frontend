import * as actionTypes from '../actions/actionTypes'

const initialState = {
    visitList: [],
    loading: false,
    error: null,
    totalPages: null,
    totalElements: null,
    pageSize: null,
    pageNumber: 0
};

const visitListReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_VISITS_SUCCESS:
            return {
                ...state, visitList: action.visitList, totalPages: action.totalPages,
                totalElements: action.totalElements, pageSize: action.pageSize,
                pageNumber: action.pageNumber, loading: false, error: null
            };
        case actionTypes.FETCH_VISITS_FAIL:
            return { ...state, loading: false, error: action.error };
        case actionTypes.FETCH_VISITS_START:
            return { ...state, loading: true };
        case actionTypes.FETCH_VISITS_INIT:
            return { ...state, error: null, visitList: null };
        default:
            return state;
    }
};

export default visitListReducer;