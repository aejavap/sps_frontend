import * as actionTypes from '../actions/actionTypes'

const initialState = {
    loading: false,
    userData: null,
    created: false,
    error: null
};

const userCreationReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_CREATION_SUCCESS:
            return {...state, loading: false, created: true, userData: action.userData, error: null};
        case actionTypes.USER_CREATION_FAIL:
            return {...state, loading: false, created: false, error: action.error, userData: null};
        case actionTypes.USER_CREATION_INIT:
            return {...state, created: false, error: null, userData: null};
        case actionTypes.USER_CREATION_START:
            return {...state, loading: true, error: null, userData: null};
        default:
            return state;
    }
};

export default userCreationReducer;
