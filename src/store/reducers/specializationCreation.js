import * as actionTypes from '../actions/actionTypes'

const initialState = {
    loading: false,
    specializationData: null,
    createdSpec: false,
    errorSpec: null
};

const specializationCreationReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SPECIALIZATION_CREATION_SUCCESS:
            return {...state, loading: false, createdSpec: true, specializationData: action.specializationData, errorSpec: null};
        case actionTypes.SPECIALIZATION_CREATION_FAIL:
            return {...state, loading: false, createdSpec: false, errorSpec: action.error};
        case actionTypes.SPECIALIZATION_CREATION_INIT:
            return {...state, createdSpec: false, errorSpec: null};
        case actionTypes.SPECIALIZATION_CREATION_START:
            return {...state, loading: true, errorSpec: null};
        default:
            return state;
    }
};

export default specializationCreationReducer;
