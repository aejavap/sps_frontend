import * as actionTypes from './actionTypes'
import axios from 'axios'
import {authLogout} from "./authentication";
import {fetchUserFail} from "./user";

/////////////////////////////////////////////////////////////
// specializationCreation                                 ///
/////////////////////////////////////////////////////////////
export const specializationCreationSuccess = specializationData => {
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_SUCCESS,///
        specializationData: specializationData            ///
    }                                                     ///
};                                                        ///
export const specializationCreationFail = error => {      ///
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_FAIL,   ///
        error: error                                      ///
    }                                                     ///
};                                                        ///
export const specializationCreationStart = () => {        ///
    return {                                              ///
        type: actionTypes.SPECIALIZATION_CREATION_START   ///
    }                                                     ///
};                                                        ///
/////////////////////////////////////////////////////////////

export const specializationCreation = (url, specializationData, token) => {
    return dispatch => {
        if (token) {
            axios.post(url, specializationData, {headers: {'Authorization': `Bearer ${token}`}})
            .then(() => {
                dispatch(specializationCreationSuccess(specializationData));
            }).catch(error => {
                    if (error.response) {
                        if (error.response.data.message && error.response.data.message.includes(`${specializationData.title} already exists.`)) {
                            dispatch(specializationCreationFail('Tokia specializacija jau yra'))
                        } else if (error.response.data.error === 'unauthorized') {
                            dispatch(specializationCreationFail('Esate neprisijungęs, po 5 sekundžių būsite nukreipti į pagrindinį puslapį, prašome prisijungti'));
                            setTimeout(() => {dispatch(authLogout());}, 7000);
                        } else {
                            dispatch(specializationCreationFail(error.response.data.message))
                        }
                    } else if (error.message === 'Network Error') {
                        dispatch(fetchUserFail('Tinklo klaida'));
                        dispatch(specializationCreationFail('Tinklo klaida'))
                    } else {
                        dispatch(fetchUserFail(error.message));
                    }
                })
        } else {
            dispatch(authLogout())
        }
    }
};

export const creationInit = () => {
    return {
        type: actionTypes.SPECIALIZATION_CREATION_INIT
    }
};
