import * as actionTypes from './actionTypes'
import axios from 'axios'
import {authLogout} from "./authentication";

/// fetchPrescriptions
export const fetchPrescriptionsSuccess = (prescriptionList, totalPages, totalElements, pageSize, pageNumber) => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_SUCCESS,
        prescriptionList: prescriptionList,
        totalPages: totalPages,
        totalElements: totalElements,
        pageSize: pageSize,
        pageNumber: pageNumber
    }
};
export const fetchPrescriptionsFail = error => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_FAIL,
        error: error
    }
};
export const fetchPrescriptionsStart = () => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_START
    }
};
export const fetchPrescriptionsInit = () => {
    return {
        type: actionTypes.FETCH_PRESCRIPTIONS_INIT
    }
};

//Fetch prescriptions using get
export const fetchPrescriptions = (url, token, pageSize) => {
    return dispatch => {
        dispatch(fetchPrescriptionsStart());
        if (token) {
            const sort = '?sort=validUntil%2Cdesc';
            axios.get(url + sort, {headers: {'Authorization': `Bearer ${token}`}, params: {'size': pageSize}})
                .then(response => {
                    const getPrescriptions = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchPrescriptionsSuccess(getPrescriptions, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchPrescriptionsFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchPrescriptionsFail('Tinklo klaida'));
                } else {
                    dispatch(fetchPrescriptionsFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};

//Fetch prescriptions using get
export const changePageFetchPrescriptions = (url, token, pageNumber, pageSize) => {
    return dispatch => {
        if (token) {
            const sort = '?sort=validUntil%2Cdesc';
            axios.get(url + sort, {headers: {'Authorization': `Bearer ${token}`}, params: {'page': pageNumber, 'size': pageSize}})
                .then(response => {
                    const getPrescriptions = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchPrescriptionsSuccess(getPrescriptions, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchPrescriptionsFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchPrescriptionsFail('Tinklo klaida'));
                } else {
                    dispatch(fetchPrescriptionsFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};
