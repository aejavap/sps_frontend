import * as actionTypes from './actionTypes';
import axios from "axios/index";
import jwtDecode from "jwt-decode";

export const authInit = () => {
    return {
        type: actionTypes.AUTH_INIT
    }
};

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
};

export const authSuccess = (token) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
    }
};

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
};

export const authUserAccount = authUserAccount => {
    return {
        type: actionTypes.AUTH_USER_ACCOUNT,
        authUser: authUserAccount
    }
};

export const authLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('date');
    return {
        type: actionTypes.AUTH_LOGOUT,
    }
};

export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout());
        }, expirationTime * 1000);
    };
};

export const authUserAccountInfo = token => {
    const decodedToken = jwtDecode(token);
    const username = decodedToken.user_name;
    return dispatch => {
        axios.get('api/me', {headers: {'Authorization': `Bearer ${token}`}})
            .then(response => {
                dispatch(authUserAccount({...response.data, username: username}))
            }).catch(error => {
            if (error.response && error.response.data.error === 'unauthorized') {
                dispatch(authFail('Esate neprisijungęs'));
                dispatch(authLogout())
            } else if (error.message === 'Network Error') {
                dispatch(authFail('Tinklo klaida'));
            } else {
                dispatch(authFail('Klaida'))
            }
        })
    }
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(authLogout());
        } else {
            const expirationDate = new Date(localStorage.getItem('date'));
            if (expirationDate < new Date()) {
                dispatch(authLogout());
            } else {
                dispatch(authUserAccountInfo(token));
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    };
};

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            grant_type: 'password',
            username: username,
            password: password,
        };
        const authHeader = btoa('sps-webapp:uJMmzskjqhKUCup9qSc8H6HQ6Vs9nqEx');
        const headers = {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${authHeader}`
        };
        const tokenUrl = 'oauth/token';
        const objectToString = JSON.stringify(authData);
        const formData = objectToString.replace(/[{} "]/g, '').replace(/:/g, '=').replace(/,/g, '&');
        axios.post(tokenUrl, formData, {headers: headers})
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expires_in * 1000);
                const token = response.data.access_token;
                localStorage.setItem('token', token);
                localStorage.setItem('date', expirationDate);
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout(response.data.expires_in));
                dispatch(authUserAccountInfo(token))
            })
            .catch(error => {
                if (error.response && error.response.data.error === 'invalid_grant') {
                    dispatch(authFail('Neteisingi prisijungimo duomenys'));
                } else {
                    dispatch(authFail('Tinklo klaida'));
                }
            });
    };
};
