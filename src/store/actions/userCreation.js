import * as actionTypes from './actionTypes'
import axios from 'axios'
import {reset} from 'redux-form'
import {authLogout} from "./authentication";
import errorMessages from './errorMessages/errorMessages';

/////////////////////////////////////////////////////////////
// userCreation                                           ///
/////////////////////////////////////////////////////////////
export const userCreationSuccess = userData => {          ///
    return {                                              ///
        type: actionTypes.USER_CREATION_SUCCESS,          ///
        userData: userData                                ///
    }                                                     ///
};                                                        ///
export const userCreationFail = error => {                ///
    return {                                              ///
        type: actionTypes.USER_CREATION_FAIL,             ///
        error: error                                      ///
    }                                                     ///
};                                                        ///
export const userCreationStart = () => {                  ///
    return {                                              ///
        type: actionTypes.USER_CREATION_START             ///
    }                                                     ///
};                                                        ///
/////////////////////////////////////////////////////////////

export const userCreation = (url, userData, token, form, type, accountData) => {
    return dispatch => {
        if (token) {
            dispatch(userCreationStart());
            axios.post(url, userData, {headers: {'Authorization': `Bearer ${token}`}, params: accountData})
                .then(response => {
                    dispatch(userCreationSuccess({...userData, id: response.data}));
                    dispatch(reset(form))})
                .catch(error => {
                    if (error && error.response && error.response.data && error.response.data.message) {
                        dispatch(userCreationFail(errorMessages(error.response.data, userData, type)));
                    } else if (error.message === 'Network Error') {
                        dispatch(userCreationFail('Tinklo klaida'));
                    } else {
                        dispatch(userCreationFail(error.message))
                    }
                })
        } else {
            dispatch(userCreationFail('Esate neprisijungęs'));
            setTimeout(() => dispatch(authLogout()), 4000);
        }
    }
};

export const userUpdate = (url, userData, token, type) => {
    return dispatch => {
        if (token) {
            dispatch(userCreationStart());
            axios.put(url, userData, {headers: {'Authorization': `Bearer ${token}`}})
                .then(() => {
                    dispatch(userCreationSuccess(userData));})
                .catch(error => {
                    if (error && error.response && error.response.data && error.response.data.message) {
                        dispatch(userCreationFail(errorMessages(error.response.data, userData, type)));
                    } else if (error.message === 'Network Error') {
                        dispatch(userCreationFail('Tinklo klaida'));
                    } else {
                        dispatch(userCreationFail(error.message))
                    }
                })
        } else {
            dispatch(authLogout())
        }
    }
};

export const creationInit = () => {
    return {
        type: actionTypes.USER_CREATION_INIT
    }
};
