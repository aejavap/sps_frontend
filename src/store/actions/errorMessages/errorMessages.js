import {toFirstUpperCaseToLowerCase} from '../../../shared/utility';

const errorMessages = (error, data, type) => {
    if (error && error.message) {
        const errorMessage = error.message;
        switch (errorMessage) {
            case `Username [${data.username}] already taken.`:
                return 'Pasirinktas vartotojo vardas yra užimtas';
            case `${data.user} with pid ${data.pid} already exists!`:
            case `${data.user} with pid ${data.pid} already exists.`:
            case `Another ${toFirstUpperCaseToLowerCase(data.user)} with pid ${data.pid} already exists.`:
                return `${type} su nurodytu asmens kodu jau egzistuoja`;
            case `No doctor with pid: ${data.doctorPid} exists!`:
            case `No doctor with pid ${data.doctorPid} exists.`:
                return 'Gydytojo su nurodytu asmens kodu nėra';
            case `No patient with pid ${data.patientPid} exists.`:
                return 'Paciento su nurodytu asmens kodu nėra';
            case `No specialization with title ${data.specializationTitle} exists`:
            case `No specialization sharing both id ${data.specializationId} and title ${data.specializationTitle} exists. Please give either id or title.`:
                return 'Pasirinktos specializacijos nėra, pasirinkite specializaciją iš sąrašo arba sukurkite naują';
            case `No diagnosis with title ${data.diagnosisTitle} exists.`:
                return 'Nurodyta diagnozė neegzistuoja';
            case `Entity with title ${data.title} already exists.`:
                return 'Tokia specializacija jau yra';
            case `Account with id [${data.id}] already exists`:
                return 'Vartotojas jau turi asmeninę paskyrą';
            case `No ingredient with title ${data.activeIngredientTitle} exists.`:
                return 'Nurodytos veikliosios medžiagos nėra';
            default:
                return errorMessage;
        }
    }
    return 'Įvyko klaida';
};

export default errorMessages;

