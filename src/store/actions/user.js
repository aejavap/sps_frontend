import * as actionTypes from './actionTypes'
import axios from 'axios'
import {authLogout} from "./authentication";

////////////////////////////////////////////////////
///               fetchUser                      ///
////////////////////////////////////////////////////
export const fetchUserSuccess = (userById) => {  ///
    return {                                     ///
        type: actionTypes.FETCH_USER_SUCCESS,    ///
        userById: userById,                      ///
    }                                            ///
};                                               ///
export const fetchUserFail = error => {          ///
    return {                                     ///
        type: actionTypes.FETCH_USER_FAIL,       ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchUserStart = () => {            ///
    return {                                     ///
        type: actionTypes.FETCH_USER_START       ///
    }                                            ///
};                                               ///
////////////////////////////////////////////////////

export const fetchUser = (url, token) => {
    return dispatch => {
        dispatch(fetchUserStart());
        if (token) {
            axios.get(url, {headers: {'Authorization': `Bearer ${token}`}})
                .then(response => {
                    const getUser = response.data;
                    if(getUser.doctorPid) {
                        const getPatientUser = {...getUser, doctorPidNullable: getUser.doctorPid};
                        dispatch(fetchUserSuccess(getPatientUser));
                    }else {
                        dispatch(fetchUserSuccess(getUser));
                    }
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchUserFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchUserFail('Tinklo klaida'));
                } else {
                    dispatch(fetchUserFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};
export const fetchUserInit = () => {
    return {
        type: actionTypes.FETCH_USER_INIT
    }
};
