import * as actionTypes from './actionTypes'
import axios from 'axios'
import {authLogout} from "./authentication";

////////////////////////////////////////////////////
///               fetchOthers                    ///
////////////////////////////////////////////////////
export const fetchOthersSuccess = (otherList, totalPages, totalElements, pageSize, pageNumber) => {
    return {                                     ///
        type: actionTypes.FETCH_OTHERS_SUCCESS,  ///
        otherList: otherList,                    ///
        totalPages: totalPages,                  ///
        totalElements: totalElements,            ///
        pageSize: pageSize,                      ///
        pageNumber: pageNumber                   ///
    }                                            ///
};                                               ///
export const fetchOthersFail = error => {        ///
    return {                                     ///
        type: actionTypes.FETCH_OTHERS_FAIL,     ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchOthersStart = () => {          ///
    return {                                     ///
        type: actionTypes.FETCH_OTHERS_START     ///
    }                                            ///
};                                               ///
export const fetchOthersInit = () => {           ///
    return {                                     ///
        type: actionTypes.FETCH_OTHERS_INIT      ///
    }                                            ///
};                                               ///
////////////////////////////////////////////////////

// Fetch others initially with loading state
export const fetchOthers = (url, token, pageSize, searchParams) => {
    return dispatch => {
        if (token) {
            // const sort = '?sort=title%2Casc';
            axios.get(url, {headers: {'Authorization': `Bearer ${token}`}, params: {'size': pageSize, 'title': searchParams}})
                .then(response => {
                    const getOthers = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchOthersSuccess(getOthers, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchOthersFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchOthersFail('Tinklo klaida'));
                } else {
                    dispatch(fetchOthersFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};

// Fetch prescriptionFills
export const fetchPrescriptionFills = (url, token, pageSize) => {
    return dispatch => {
        // dispatch(fetchOthersStart()); // only difference vs changePageFetchOthers
        if (token) {
            // const sort = '?sort=title%2Casc';
            axios.get(url, {headers: {'Authorization': 'Bearer ' + token}, params: {'size': pageSize}})
                .then(response => {
                    const getOthers = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchOthersSuccess(getOthers, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchOthersFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchOthersFail('Tinklo klaida'));
                } else {
                    dispatch(fetchOthersFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};
