import * as actionTypes from './actionTypes'
import axios from 'axios'
import {authLogout} from "./authentication";

////////////////////////////////////////////////////
///               fetchVisits                    ///
////////////////////////////////////////////////////
export const fetchVisitsSuccess = (visitList, totalPages, totalElements, pageSize, pageNumber) => {
    return {                                     ///
        type: actionTypes.FETCH_VISITS_SUCCESS,  ///
        visitList: visitList,                    ///
        totalPages: totalPages,                  ///
        totalElements: totalElements,            ///
        pageSize: pageSize,                      ///
        pageNumber: pageNumber                   ///
    }                                            ///
};                                               ///
export const fetchVisitsFail = error => {        ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_FAIL,     ///
        error: error                             ///
    }                                            ///
};                                               ///
export const fetchVisitsStart = () => {          ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_START     ///
    }                                            ///
};                                               ///
export const fetchVisitsInit = () => {           ///
    return {                                     ///
        type: actionTypes.FETCH_VISITS_INIT      ///
    }                                            ///
};                                               ///
////////////////////////////////////////////////////

//Fetch visits using get
export const fetchVisits = (url, token, pageSize) => {
    return dispatch => {
        dispatch(fetchVisitsStart());
        if (token) {
            const sort = '?sort=medicalHistoryPK.timestamp%2Cdesc';
            axios.get(url + sort, {headers: {'Authorization': `Bearer ${token}`}, params: {'size': pageSize}})
                .then(response => {
                    const getVisits = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchVisitsSuccess(getVisits, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchVisitsFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchVisitsFail('Tinklo klaida'));
                } else {
                    dispatch(fetchVisitsFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};

//Fetch visits using get
export const changePageFetchVisits = (url, token, pageNumber, pageSize) => {
    return dispatch => {
        if (token) {
            const sort = '?sort=medicalHistoryPK.timestamp%2Cdesc';
            axios.get(url + sort, {headers: {'Authorization': `Bearer ${token}`}, params: {'page': pageNumber, 'size': pageSize}})
                .then(response => {
                    const getVisits = response.data.content;
                    const totalPages = response.data.totalPages;
                    const totalElements = response.data.totalElements; //total count of objects
                    const pageSize = response.data.size; // page size
                    const pageNumber = response.data.number; // page number
                    dispatch(fetchVisitsSuccess(getVisits, totalPages, totalElements, pageSize, pageNumber));
                }).catch(error => {
                if (error.response && error.response.data.error === 'unauthorized') {
                    dispatch(fetchVisitsFail('Esate neprisijungęs'));
                    dispatch(authLogout())
                } else if (error.message === 'Network Error') {
                    dispatch(fetchVisitsFail('Tinklo klaida'));
                } else {
                    dispatch(fetchVisitsFail(error.message))
                }
            })
        } else {
            dispatch(authLogout())
        }
    }
};
