import React, { Component } from 'react';
import './UserLayout.css';
import UserNavigation from "./UserNavigation";

class UserLayout extends Component {

    render() {
        return (
            <div id='all'>
                <nav className='sidebarNavigation'>
                    <UserNavigation show={this.props.show} clicked={this.props.clicked}/>
                </nav>
                <main className="UserLayout">
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default UserLayout;
