import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';

import NavigationItems from './NavigationItems/NavigationItems';

const navbar = (props) => (
    <div>
        <Navbar fixedTop fluid collapseOnSelect>
            <Navbar.Header>
                <Navbar.Brand >
                    <Link id='mainPageSPS' to="/">
                            SPS
                        </Link>
                </Navbar.Brand>
                <Navbar.Text pullLeft id='loggedUser'>
                    {props.isAuthenticated ? `${props.userType}: ${props.firstName} ${props.lastName}` : ''}
                </Navbar.Text >
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav pullRight>
                    <NavigationItems isAuthenticated={props.isAuthenticated} />
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    </div>
);

export default navbar;
