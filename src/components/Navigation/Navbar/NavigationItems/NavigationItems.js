import React from 'react';
import './NavigationItems.css';
import RouteNavItem from './RouteNavItem/RouteNavItem';

const navigationItems = (props) => (
    <div>
        <ul id='navigationItems' className="NavigationItems">
            {!props.isAuthenticated ?
                <RouteNavItem id='navigationItemLogin' to="/login">Prisijungti</RouteNavItem> :
                <RouteNavItem id='navigationItemLogout' to="/logout">Atsijungti</RouteNavItem>
            }
            <RouteNavItem id='navigationItemStatistics' to="/statistics">Statistika</RouteNavItem>
        </ul>
    </div>

);

export default navigationItems;
