import React from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import Aux from "../../../hoc/Aux";
import './UserCard.css';

const UserCard = (props) => {
    return (
        <Aux >
            <Grid style={{margin: 'auto', alignItems: 'center', backgroundColor: 'white'}}>
                <Row className="UserCard" style={{margin: 'auto'}}>
                    <Col md={4}>
                        <img id='userCardImage' src={props.image} alt="icon" />
                    </Col>
                    <Col md={8}>
                        <div>
                            <strong><span id='userCardFirstName'>{props.firstName}</span>
                            {' '}
                            <span id='userCardLastName'>{props.lastName}</span></strong>
                        </div>
                        <div id='userCardPid'>{props.pid}</div>
                        <div>{props.children}</div>
                    </Col>
                </Row>
            </Grid>
        </Aux>

    )
};

export default UserCard;
