import React from 'react';
import Aux from "../../../hoc/Aux";
import {Table} from "react-bootstrap";
import {userProperty} from '../../../shared/utility'

const userDetails = (props) => {

    const user = props.user;
    const userProps = Object.keys(user).map(value => (
            <tr id='user' key={value}>
                <td style={{width: '35%'}} className="success" id={`user${value}`}>{userProperty(value)}</td>
                <td id={`user${user[value]}`}>{user[value]}</td>
            </tr>
        )
    );

    let userAccount = null;
    let userAccountProps = null;
    if (props.userAccount) {
        userAccount = props.userAccount;
        userAccountProps = Object.keys(userAccount).map(value => (
                <tr id='userAccount' key={value}>
                    <td className="success" id={`userAccount${value}`}>{userProperty(value)}</td>
                    <td id={`userAccount${userAccount[value]}`}>{userAccount[value]}</td>
                </tr>
            )
        );
    }

    return (
        <Aux>
            <div className="container" style={{marginTop: '25px', width: '70%'}}>
                <Table striped condensed hover responsive className="table">
                    <thead id='userDetailsHeader'>
                    <tr>
                        <th colSpan={2} id='userDetails'>VARTOTOJO INFORMACIJA</th>
                    </tr>
                    </thead>
                    <tbody>{userProps}</tbody>
                    {props.userAccount ?
                        <Aux>
                            <thead id='userAccountDetailsHeader'>
                            <tr>
                                <th colSpan={2} id='userAccountDetails'>VARTOTOJO PASKYROS INFORMACIJA</th>
                            </tr>
                            </thead>
                            <tbody>{userAccountProps}</tbody>
                        </Aux> : null}
                </Table>
            </div>
        </Aux>
    )
};

export default userDetails;
