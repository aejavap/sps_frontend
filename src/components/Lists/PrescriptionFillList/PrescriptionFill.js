import React from 'react';

const PrescriptionFill = (props) => (
    <tr id='prescriptionFill'>
        <td id='date'>{props.date}</td>
        <td id='pharmacistFullName'>{props.phFirstName}{' '}{props.phLastName}</td>
    </tr>
);

export default PrescriptionFill;
