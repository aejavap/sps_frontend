import React, { PureComponent } from 'react';
import { Table } from "react-bootstrap";

import PrescriptionFill from "./PrescriptionFill";


class PrescriptionFillList extends PureComponent {

    render() {
        const prescriptionFills = this.props.prescriptionFillList;

        const listItems = prescriptionFills.map((prescriptionFill, index) =>

            <PrescriptionFill
                key={prescriptionFill.date}
                date={prescriptionFill.date}
                phFirstName={prescriptionFill.phFirstName}
                phLastName={prescriptionFill.phLastName} 
                />

        );
        return (
            <div id='prescriptionFillList'>
                <Table striped condensed hover responsive>
                    <thead id='prescriptionFillListHeader'>
                        <tr>
                            <th id='prescriptionFillDate'>Panaudojimo data</th>
                            <th id='pharmacistFullName'>Vaistininkas</th>
                        </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default PrescriptionFillList;