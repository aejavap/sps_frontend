import React from 'react';
import {Button} from "react-bootstrap";

const Visit = (props) => (
        <tr id='visit'>
            <td id='visitDate'>{props.date}</td>
            <td id='diagnosisTitle'>{props.diagnosisTitle}</td>
            <td id='doctorFullName'>{props.doctorFirstName}{' '}{props.doctorLastName}</td>
            <td><Button id="buttonShow" bsStyle="success" onClick={props.onClick}>Detaliau</Button></td>
        </tr>
    );

export default Visit;