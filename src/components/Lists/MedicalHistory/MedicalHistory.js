import React, { Component } from 'react';
import { Table } from "react-bootstrap";
import '../List.css';
import Visit from './Visit';
import moment from "moment";

class MedicalHistory extends Component {

    render() {
        const visits = this.props.visitList;
        const listItems = visits.map((visit, index) =>
            <Visit
                date={moment(visit.date).format("YYYY-MM-DD")}
                key={visit.date + visit.diagnosisId + Math.floor(Math.random() * 10)}
                diagnosisTitle={visit.diagnosisTitle}
                doctorFirstName={visit.doctorFirstName}
                doctorLastName={visit.doctorLastName}
                onClick={() => this.props.visitDetails(index)}
            />
        );
        return (
            <div id="medicalHistoryList" className='tableList'>
                <Table striped condensed hover responsive>
                    <thead id='medicalHistoryListHeader'>
                        <tr>
                            <th id="visitDateHeader">Vizito data</th>
                            <th id="diagnosisIdHeader">Ligos kodas</th>
                            <th id="doctorFullNameHeader">Gydytojas</th>
                            <th id="visitButtonHeader"/>
                        </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default MedicalHistory;
