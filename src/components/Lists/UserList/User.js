import React from 'react';
import {Button} from "react-bootstrap";

const User = (props) => (
    <tr id='user'>
        <td id='userPidLink' onClick={props.userDetails}>{props.pid}</td>
        <td id='userFirstName'>{props.firstName}</td>
        <td id='userLastName'>{props.lastName}</td>
        <td><Button id="buttonEdit" bsStyle="warning" onClick={props.userEdit}>Redaguoti</Button></td>
        {props.patient ? <td><Button id="buttonAccount" bsStyle="success" disabled={props.hasAccount} onClick={props.userAccount}>{props.hasAccount ? 'Paskyra yra' : 'Pridėti'}</Button></td> : null}
    </tr>
);

export default User;
