import React from 'react';
import { Button } from "react-bootstrap";
import "./Prescription.css"

const Prescription = (props) => (
    (props.validUntil < props.currentDate) ?
        <tr id='prescription' className="inactive">
            {(props.validUntil < "2999-12-31") ? <td id='validUntil'>{props.validUntil}</td> : <td id='validUntil'>Neterminuotas</td>}
            <td id='prescribedOn'>{props.prescribedOn}</td>
            <td id='activeIngredient'>{props.activeIngredient}</td>
            {props.canSeePatient ? <td id="prescriptionFillNum">{props.fillsNo}</td> : null}
            <td id='prescriptionButton'><Button id="buttonShow" bsStyle="success" onClick={props.onClick}>{props.buttonTitle}</Button></td>
        </tr>
        : <tr id='prescription'>
            {(props.validUntil < "2999-12-31") ? <td id='validUntil'>{props.validUntil}</td> : <td id='validUntil'>Neterminuotas</td>}
            <td id='prescribedOn'>{props.prescribedOn}</td>
            <td id='activeIngredient'>{props.activeIngredient}</td>
            {props.canSeePatient ? <td id="prescriptionFillNum">{props.fillsNo}</td> : null}
            <td id='prescriptionButton'><Button id="buttonShow" bsStyle="success" onClick={props.onClick}>{props.buttonTitle}</Button></td>
        </tr>
);

export default Prescription;
