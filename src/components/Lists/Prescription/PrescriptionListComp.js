import React, {PureComponent} from 'react';
import {Table} from "react-bootstrap";
import Prescription from "./Prescription";
import '../List.css';

class PrescriptionList extends PureComponent {

    render() {

        const prescriptions = this.props.prescriptionList;

        const listItems = prescriptions.map((prescription, index) =>

            <Prescription
                key={prescription.prescriptionId}
                validUntil={prescription.validUntil}
                prescribedOn={prescription.prescriptionDate}
                activeIngredient={prescription.activeIngredientTitle}
                fillsNo={prescription.fillsNo}
                canSeePatient={this.props.canSeePatient}
                currentDate={this.props.currentDate}
                onClick={() => this.props.prescriptionAction(index)}
                buttonTitle={this.props.buttonTitle}
            />
        );
        return (
            <div id='prescriptionList' className='tableList'>
                <Table striped condensed hover responsive>
                    <thead id='prescriptionListHeader'>
                    <tr>
                        <th id='prescriptionValidUntilHeader'>Galioja iki:</th>
                        <th id='prescriptionPrescribedOnHeader'>Išrašytas:</th>
                        <th id='prescriptionActiveIngredientHeader'>Veiklioji medžiaga</th>
                        {this.props.canSeePatient ? <th id='prescriptionFillNumHeader'>Panaudota kartų</th> : null}
                        <th id='prescriptionButtonHeader'/>
                    </tr>
                    </thead>
                    <tbody>{listItems}</tbody>
                </Table>
            </div>
        )
    }
}

export default PrescriptionList;
