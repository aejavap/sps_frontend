import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderCheckbox, renderTextField} from './renderFields'
import {Button} from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import {connect} from "react-redux";
import {Paper} from 'material-ui';

let accountForm = props => {
    const {handleSubmit, pristine, submitting} = props;
    return (
        <form id='accountForm' onSubmit={handleSubmit}>
            <h3 id='accountFormHeader'>Vartotojo paskyros sukurimas</h3>
            <Paper className='formPaper' zDepth={2}>
                <Field name="firstName" disabled={true} component={renderTextField} label="Vardas" />
                <Field name="lastName" disabled={true} component={renderTextField} label="Pavardė" />
                <Field name="pid" disabled={true} component={renderTextField} label="Asmens kodas" />
                <Field autoFocus={true} name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type={props.showPassword ? "text" : 'password'} component={renderTextField} label="Slaptažodis"/>
                <Field name="showPassword" component={renderCheckbox} label="Rodyti slaptažodį?"/>
                <div id='accountFormError' className='error'>{props.children}</div>
            </Paper>
            <div>
                <Button id='accountFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Priskirti</Button>
                {' '}
                <Button id='accountFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.back}>Atšaukti</Button>
            </div>
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

accountForm = reduxForm({form: 'AccountForm', validate})(accountForm);
export default connect(mapStateToProps)(accountForm)
