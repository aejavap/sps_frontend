import React from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {renderCheckbox, renderTextField, renderMultiLineTextField} from "../../../components/Form/Forms/renderFields";
import {Button} from "react-bootstrap";
import Aux from "../../../hoc/Aux"
import validate from "../../../shared/validate";
import {Paper} from 'material-ui';
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../../shared/utility'
import {renderAutoComplete} from "./renderFields";

let medicalHistoryRecordForm = props => {

    const {handleSubmit, pristine, submitting, reset} = props;
    return (
        <Aux>
            <form id='medicalHistoryRecordForm' onSubmit={handleSubmit}>
                <h3 id='medicalHistoryRecordFormHeader'>Naujas ligos įrašas</h3>
                <Paper className='formPaper' zDepth={2}>
                    <Field name="firstName" autoFocus={!props.myPatient} disabled={props.myPatient} normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Paciento vardas"/>
                    <Field name="lastName" disabled={props.myPatient} normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Paciento pavardė"/>
                    <Field name="patientPid" disabled={props.myPatient} normalize={onlyNumbers} maxLength='11' component={renderTextField} label="Paciento asmens kodas"/>
                    <Field autoFocus={props.myPatient} name="diagnosisTitle" onChange={(event, value) => props.diagnosisChange(event, value)}
                           component={renderAutoComplete} label="Diagnozės kodas" maxLength='6' dataSource={[].concat(props.diagnosis)} />
                    <Field name="appointmentLength" normalize={onlyNumbers} maxLength='3' hintText="Vizito trukmė minutėmis" component={renderTextField} label="Vizito trukmė"/>
                    <Field name="compensated" component={renderCheckbox} label="Kompensuojamas?"/>
                    <Field name="repeatVisitation" component={renderCheckbox} label="Pakartotinis vizitas?"/>
                    <Field style={{ textAlign: 'left', width: '400px' }} name="notes" maxLength='8001' component={renderMultiLineTextField} label="Pastabos"/>
                    <div id='medicalHistoryRecordFormError' className='error'>{props.children}</div>
                </Paper>
                <div>
                    <Button id='medicalHistoryRecordButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Registruoti</Button>
                    {' '}
                    <Button id='medicalHistoryRecordButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.myPatient ? props.back : reset}>Atšaukti</Button>
                </div>
            </form>
        </Aux>
    )
};

const mapStateToProps = (state, index) => {
    if(state.fetch.userList){
        return {
            initialValues: state.fetch.userList[index] // pull initial values from account reducer
        }
    } else if (state.getUser.userById) {
        return {
            initialValues: state.getUser.userById
        }
    } return {};
};

medicalHistoryRecordForm = reduxForm({form: 'MedicalHistoryRecordForm', validate})(medicalHistoryRecordForm);
export default connect(mapStateToProps)(medicalHistoryRecordForm)
