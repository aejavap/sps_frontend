import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import { Button } from 'react-bootstrap';

import { renderDatePickerField } from '../../../components/Form/Forms/renderFields'
import validate from "../../../shared/validate";
import Aux from "../../../hoc/Aux"

let dateRange = props => {
    const { handleSubmit, pristine, submitting, value } = props;

    return (
        <Aux>
            <form id="dateRange" onSubmit={handleSubmit}>
                <h4>Pasirinkite laikotarpį:</h4>
                <Field name="dateFrom" minDate={new Date(new Date().setFullYear(new Date().getFullYear() - 1))} maxDate={new Date()} component={renderDatePickerField} label="Nuo" />
                <Field name="dateUntil" minDate={new Date(new Date().setFullYear(new Date().getFullYear() - 1))} maxDate={new Date()} component={renderDatePickerField} label="Iki" />
                {' '}
                <Button bsStyle="info" type="submit" id="dateRangeSearch" disabled={pristine || submitting}>
                    Formuoti
            </Button>
            </form>
        </Aux>
    );
};
export default dateRange = reduxForm({ form: 'DateRange', validate })(dateRange)
