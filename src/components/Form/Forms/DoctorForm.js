import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {renderTextField, renderAutoComplete, renderCheckbox} from './renderFields';
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css';
import { connect } from "react-redux";
import {Paper} from 'material-ui';
import Aux from "../../../hoc/Aux";
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../../shared/utility'

let doctorForm = props => {

    const {handleSubmit, pristine, submitting, reset } = props;
    return (
        <form id='doctorForm' onSubmit={handleSubmit}>
            <h3 id='doctorFormHeader'>{props.created ? 'Gydytojo registracija' : 'Atnaujinti gydytojo duomenis'}</h3>
            <Paper className='formPaper' zDepth={2}>
                <Field autoFocus={true} name="firstName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Vardas" />
                <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} component={renderTextField} label="Pavardė" />
                <Field name="pid" maxLength='11' normalize={onlyNumbers} component={renderTextField} label="Asmens kodas" />
                <Field name="specializationTitle" component={renderAutoComplete} label="Specializacija" hintText='Jei nėra, įrašykite "Kita"'
                       onChange={(event, value) => props.specializationChange(event, value)} dataSource={[].concat(props.spec).concat('Kita')} />
                {props.created ? <Aux><Field name="username" component={renderTextField} label="Prisijungimo vardas" />
                <Field name="password" type={props.showPassword ? "text" : 'password'} component={renderTextField} label="Slaptažodis"/>
                <Field name="showPassword" component={renderCheckbox} label="Rodyti slaptažodį?"/></Aux> : null}
                <div id='doctorFormError' className='error'>{props.children}</div>
            </Paper>
            {props.showButtons ?
            <Aux>
                <Button id='doctorFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>{props.created ? 'Registruoti':'Atnaujinti'}</Button>
                {' '}
                <Button id='doctorFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={props.created ? pristine || submitting : submitting} onClick={props.created ? reset : props.back}>Atšaukti</Button></Aux> : null}
        </form>
    )
};

const mapStateToProps = state => {
    return {
        initialValues: state.getUser.userById // pull initial values from account reducer
    }
};

doctorForm = reduxForm({ form: 'DoctorForm', validate})(doctorForm);
export default connect(mapStateToProps)(doctorForm)
