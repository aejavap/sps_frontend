import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderTextField} from './renderFields'
import {Button} from "react-bootstrap";
import validate from "../../../shared/validate";
import './UserForms.css'
import {Paper} from "material-ui";
import Aux from "../../../hoc/Aux";

const accountChangeForm = props => {
    const {handleSubmit, pristine, submitting} = props;
    return (
        <form id='passwordChangeForm' onSubmit={handleSubmit}>
            <Paper className='formPaper' zDepth={2}>
                <h3 id='passwordChangeFormHeader'>{props.changePassword ? 'Slaptažodžio keitimo forma' : 'Vartotojo vardo keitimo forma'}</h3>
                {!props.changePassword ? <Field name="username" autoFocus={true} component={renderTextField} label='Įveskite naują vartotojo vardą'/> : null}
                <Field autoFocus={props.changePassword} name="password" type="password" hintText="Įveskite slaptažodį" component={renderTextField} label="Slaptažodis"/>
                {props.changePassword ? <Aux>
                    <Field name="newPassword" type="password" hintText="Įveskite naują slaptažodį" component={renderTextField} label="Naujas slaptažodis"/>
                        <Field name="newPasswordConfirmation" type="password" hintText="Įveskite naują slaptažodį" component={renderTextField} label="Pakartokite naują slaptažodį"/>
                </Aux> : <p style={{color: 'orange'}}>Pakeitus vartotojo vardą reikės iš naujo prisijungti.</p>}

                <div id='passwordChangeFormError' className='error'>{props.children}</div>
                <div id='passwordChangeFormButtons'>
                    <Button id='passwordChangeFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Atnaujinti</Button>
                    {' '}
                    <Button id='passwordChangeFormDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.back}>Atšaukti</Button>
                </div>
            </Paper>
        </form>
    )
};

export default reduxForm({form: 'AccountChangeForm', validate})(accountChangeForm);
