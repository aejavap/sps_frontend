import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import {MenuItem, Paper} from 'material-ui';
import { renderTextField, renderMultiLineTextField, renderSelectField, renderDatePickerField } from './../../Form/Forms/renderFields';
import { Button } from "react-bootstrap";
import validate from "../../../shared/validate";
import Aux from "../../../hoc/Aux";
import {onlyNumbers, onlyFloatNumbers, toFirstUpperCaseToLowerCase} from '../../../shared/utility';
import {renderAutoComplete, renderCheckbox} from "./renderFields";
import './UserForms.css';

let prescriptionForm = (props) => {

    const { handleSubmit, pristine, submitting, reset } = props;
    return (
        <Aux>
            <form id='prescriptionForm' onSubmit={handleSubmit}>
                <h3 id='prescriptionFormHeader'>Naujas receptas</h3>
                <Paper className='formPaper' zDepth={2}>
                    <Field name="firstName" autoFocus={!props.myPatient} normalize={toFirstUpperCaseToLowerCase} disabled={props.myPatient} component={renderTextField} label="Paciento vardas"/>
                    <Field name="lastName" normalize={toFirstUpperCaseToLowerCase} disabled={props.myPatient} component={renderTextField} label="Paciento pavardė" />
                    <Field name="patientPid" normalize={onlyNumbers} disabled={props.myPatient} maxLength='11' component={renderTextField} label="Paciento asmens kodas" />
                    <Field autoFocus={props.myPatient} name="activeIngredientTitle" onChange={(event, value) => props.ingredientChange(event, value)}
                           component={renderAutoComplete} label="Vaisto veiklioji medžiaga" dataSource={[].concat(props.ingredient)} />
                    <Field name="activeIngredientPerDose" normalize={onlyFloatNumbers} component={renderTextField} label="Medžiagos kiekis dozėje" />
                    <Field style={{ textAlign: 'left', width: '400px' }} name="activeIngredientUnits" component={renderSelectField} label="Matavimo vienetas">
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseMg' value="mg" primaryText="mg" />
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseµg' value="µg" primaryText="µg" />
                        <MenuItem id='prescriptionFormActiveIngredientPerDoseUi' value="TV/IU" primaryText="TV/IU" />
                    </Field>
                    <Field name="validUntil" minDate={new Date()} component={renderDatePickerField} label="Galioja iki:" />
                    <Field name="unlimitedValidity" component={renderCheckbox} label="Neterminuotas?"/>
                    <Field style={{ textAlign: 'left', width: '400px' }} name="dosageNotes" maxLength='8001' component={renderMultiLineTextField} label="Vartojimo aprašymas" />
                    <div id='prescriptionFormError' className='error'>{props.children}</div>
                </Paper>
                <div>
                    <Button id='prescriptionFormButtonSuccess' bsStyle="success" bsSize='lg' type="submit" disabled={pristine || submitting}>Registruoti</Button>
                    {' '}
                    <Button id='prescriptionFormButtonDanger' bsStyle="danger" bsSize='lg' type="button" disabled={submitting} onClick={props.myPatient ? props.back : reset}>Atšaukti</Button>
                </div>
            </form>
        </Aux>
    )
};


const mapStateToProps = (state, index) => {
    if (state.fetch.userList){
        return {
            initialValues: state.fetch.userList[index] // pull initial values from account reducer
        }
    } else if (state.getUser.userById) {
        return {
            initialValues: state.getUser.userById
        }
    } return {};
};

prescriptionForm = reduxForm({ form: 'PrescriptionForm', validate })(prescriptionForm);
export default connect(mapStateToProps)(prescriptionForm)

