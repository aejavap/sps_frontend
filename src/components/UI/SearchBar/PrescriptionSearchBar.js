import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderTextField } from '../../../components/Form/Forms/renderFields'
import { Button } from 'react-bootstrap';
import validate from "../../../shared/validate";
// import { onlyNumbers } from '../../../shared/utility'

let prescriptionSearchBar = props => {
    const { handleSubmit, pristine, submitting, value } = props;
    return (
        <form id="prescriptionSearchForm" onSubmit={handleSubmit}>
            <Field
                name="pid"
                id="pharmacistPatientPidSearch"
                component={renderTextField}
                value={value}
                label="Įveskite asmens kodą"
                style={{ width: '350px' }}
                autoFocus={true}
                maxLength='11'
            />
            {' '}
            <Button bsStyle="info" type="submit" id="pharmacistPatientPidSearchButton" disabled={pristine || submitting}>
                Ieškoti
            </Button>
        </form>
    );
};
export default prescriptionSearchBar = reduxForm({ form: 'PrescriptionSearchBar', validate })(prescriptionSearchBar)
