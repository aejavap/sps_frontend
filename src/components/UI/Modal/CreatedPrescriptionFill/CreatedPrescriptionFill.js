import React from 'react';
import '../../Modal/CreatedUser/CreatedUser.css';

const CreatedPrescriptionFill = (props) => {
    return (
        <div className="creationModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">Panaudojimas užfiksuotas</h4>
                    </div>
                    <div className="modal-footer">
                        <button onClick={props.hide} type="button" className="btn btn-secondary" data-dismiss="modal">Uždaryti</button>

                    </div>
                </div>
            </div>
        </div>
    )
};

export default CreatedPrescriptionFill;