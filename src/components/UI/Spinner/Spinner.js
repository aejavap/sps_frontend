import React from 'react';
import './Spinner.css'

const spinner = () => (
<div className="fa-5x">
  <i className="fas fa-circle-notch fa-spin"></i>
  </div>
);

export default spinner;
