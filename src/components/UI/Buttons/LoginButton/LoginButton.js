import React from 'react';
import { Link } from 'react-router-dom';
import './LoginButton.css';
import patient from '../../../../assets/patient.svg';
import doctor from '../../../../assets/doctor.svg';
import pharmacist from '../../../../assets/pharmacist.svg';

const loginButton = () => {
    return (
        <div id='loginButtons' className="LoginGroup">
            <ul>
                <li>
                    <Link to="/login" style={{ textDecoration: "none" }}>
                        <div id='loginButtonPatient' className="login-card">
                            <span>
                                <img src={patient} alt={"pacientas"} />
                            </span>
                            <p className="user-type">Pacientas</p>
                        </div>
                    </Link>
                </li>

                <li>
                    <Link to="/login" style={{ textDecoration: "none" }}>
                        <div id='loginButtonDoctor' className="login-card">
                            <span>
                                <img src={doctor} alt={"gydytojas"} />
                            </span>
                            <p className="user-type">Gydytojas</p>
                        </div>
                    </Link>
                </li>

                <li>
                    <Link to="/login" style={{ textDecoration: "none" }}>
                        <div id='loginButtonPharmacist' className="login-card">
                            <span>
                                <img src={pharmacist} alt={"vaistininkas"} />
                            </span>
                            <p className="user-type">Vaistininkas</p>
                        </div>
                    </Link>
                </li>
            </ul>
        </div>
    )
};

export default loginButton;
