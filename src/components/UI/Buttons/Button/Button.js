import React from 'react';
import { Link } from 'react-router-dom';
import './Button.css';

const button = (props) => (
    <Link to={props.link}>
    <button
        className={['Button', [props.btnType]].join(' ')}
        onClick={props.clicked}
    >
        {props.children}
    </button>
    </Link>
);

export default button;
