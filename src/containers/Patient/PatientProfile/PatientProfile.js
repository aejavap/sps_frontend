import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from "react-redux";
import {Redirect} from "react-router-dom";
import Aux from "../../../hoc/Aux";
import Spinner from "../../../components/UI/Spinner/Spinner";
import UserCard from '../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../assets/usericon-guy.png';
import AccountChange from "../../Administration/UserAccount/AccountChange";

class PatientProfile extends Component {
    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: 0,
            dob: ''
        },
        showPasswordForm: false,
        showUsernameForm: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = this.props.userDetailsRedux;
            const updatedUser = { ...authUser, image: usericon, loading: false };
            this.setState({ user: updatedUser })
        }
    }

    changePasswordHandler = () => this.setState({
        showPasswordForm: !this.state.showPasswordForm,
        showUsernameForm: false
    });
    changeUsernameHandler = () => this.setState({
        showUsernameForm: !this.state.showUsernameForm,
        showPasswordForm: false
    });

    render() {
        const buttonStyle = {width: '150px'};
        let changePasswordForm = null;
        if (this.state.showPasswordForm) {
            changePasswordForm = <AccountChange back={this.changePasswordHandler} typePassword/>
        } else if (this.state.showUsernameForm) {
            changePasswordForm = <AccountChange back={this.changeUsernameHandler}/>
        }
        let userCard = null;
        if (!this.props.userDetailsRedux) {
            userCard = <Redirect to='/patient/home' />
        } else if (this.state.loading || this.props.loadingRedux) {
            userCard = <Spinner />
        } else if (this.state.error || this.props.errorRedux) {
            userCard = <h3>Tinklo klaida</h3>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                Gimimo data: {this.state.user.dob}
                <p>
                    <Button id='adminChangePasswordButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
                <p>
                    <Button id='adminChangeUsernameButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changeUsernameHandler}>
                        Keisti vartotojo vardą
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(PatientProfile);
