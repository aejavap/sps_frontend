import React, { Component } from 'react';
import { connect } from "react-redux";
import * as actions from '../../../../store/actions/dispatchActions';
import Aux from "../../../../hoc/Aux";
import Spinner from "../../../../components/UI/Spinner/Spinner";
import SearchBar from '../../../../components/UI/SearchBar/PrescriptionSearchBar';
import UserCard from "../../../../components/DetailCards/UserCard/UserCard";
import {Link} from "react-router-dom";
import {Button} from "react-bootstrap";
import usericon from '../../../../assets/usericon.png';

class SearchPatient extends Component {
    state = {
        image: usericon,
        searchPid: '',
        error: null,
    };
    defaultUrl = 'api/doctor/patient/';

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    searchHandler = value => {
        const searchPid = value.pid;
        const url = this.defaultUrl + searchPid;
        const token = this.props.tokenRedux;
        this.props.onFetchUser(url, token);
    };

    render() {
        const searchPid = this.state.searchPid;
        let userCard = null;
        let errorMessage = null;

        if (this.props.loadingRedux) {
            userCard = <Spinner />;
        }

        if (this.props.errorRedux) {
            errorMessage = <h4>Paciento su nurodytu asmens kodu nėra.</h4>
        } else if (this.props.foundUser)  {
            const user = this.props.foundUser;
            const buttonStyle = {width: '150px'};
            userCard = <UserCard
                image={this.state.image}
                firstName={user.firstName}
                lastName={user.lastName}
                pid={user.pid}>
                <p><Link to={`/doctor/newrecord/${user.pid}`}>
                    <Button bsStyle="info" id='foundNewRecord' style={buttonStyle} >
                        Naujas ligos įrašas
                    </Button>
                </Link></p>
                <p><Link to={`/doctor/newprescription/${user.pid}`}>
                    <Button bsStyle="info" id='foundNewPrescription' style={buttonStyle} >
                        Naujas receptas
                    </Button>
                </Link></p>
            </UserCard>
        }

        return (
            <Aux>
                <h3>Pacientų paieška</h3>
                <div style={{marginBottom: '50px'}}>
                    <SearchBar value={searchPid} onSubmit={this.searchHandler}/>
                </div>
                <br />
                <div>
                    {userCard}
                    {errorMessage}
                </div>
            </Aux>
        );
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        foundUser: state.getUser.userById,
        loadingRedux: state.getUser.loading,
        errorRedux: state.getUser.error,
        tokenRedux: state.auth.accessToken,
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPatient);

