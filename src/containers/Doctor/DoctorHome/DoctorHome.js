import React, {Component} from 'react';
import Aux from '../../../hoc/Aux';
import PatientList from "../../../containers/Lists/PatientList/PatientList";
import DoctorSearchPatient from "./DoctorSearchPatient/DoctorSearchPatient";
import './DoctorHome.css';

class DoctorHome extends Component {

    state = {
        findPatient: false
    };

    findPatientHandler = () => this.setState({findPatient: false});
    myPatientHandler = () => this.setState({findPatient: true});
    render() {

        let list = <PatientList/>;

        if (this.state.findPatient){
            list = <DoctorSearchPatient/>
        }

        return (
            <Aux>
                <div>
                    <button id='doctorHomeMyPatients' className={this.state.findPatient ? 'change' : 'change activeA'} disabled={!this.state.findPatient} onClick={this.findPatientHandler}>Mano pacientai</button>
                    <button id='doctorHomeSearchPatients' className={!this.state.findPatient ? 'change' : 'change activeA'} disabled={this.state.findPatient} onClick={this.myPatientHandler} >Visų pacientų paieška</button>
                </div>
                {list}
            </Aux>
        );
    }
}

export default DoctorHome;
