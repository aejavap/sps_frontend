import React, {Component} from 'react';
import MedicalHistoryRecordForm from "../../../../components/Form/Forms/MedicalHistoryRecordForm";
import Aux from "../../../../hoc/Aux";
import {connect} from "react-redux";
import {initialize, registerField} from 'redux-form';
import * as actions from "../../../../store/actions/dispatchActions";
import Modal from "../../../../components/UI/Modal/Modal";
import CreatedVisit from "../../../../components/UI/Modal/CreatedVisit/CreatedVisit";
import icon from '../../../../assets/usericon-guy.png';
import moment from 'moment';

class MedicalHistoryRecord extends Component {

    state = {
        isCreated: false,
        image: icon
    };
    diagnosisUrl = 'api/diagnoses/search/by-title';
    url = 'api/doctor/new/medical-record';

    componentWillMount() {
        const url = this.diagnosisUrl;
        const token = this.props.tokenRedux;
        this.props.onFetchDiagnoses(url, token, 3, 'A');
        this.props.onInitCreation();
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        this.props.regFieldPatientPid();
        this.props.regFieldFirstName();
        this.props.regFieldLastName();
        this.props.regFieldNotes();
        this.props.regFieldDiagnosisTitle();
        this.props.regFieldAppointmentLength();
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined' && this.props.userRedux && this.props.authDoctor) {
            const values = this.props.userRedux[number];
            const doctorId = this.props.authDoctor.id;
            const data = {...values, patientPid: values.pid, doctorId: doctorId};
            this.props.onLoad(data);
        } else if (typeof number !== 'undefined' && this.props.notMinePatient && this.props.authDoctor) {
            const patient = this.props.notMinePatient;
            const data = {...patient, patientPid: patient.pid};
            this.props.onLoad(data);
        } else this.props.history.replace('/doctor/newrecord')
    }

    medicalHistoryRecordCreationHandler = values => {
        this.setState({isCreated: true});
        let url = this.url;
        const token = this.props.tokenRedux;
        const formName = 'MedicalHistoryRecordForm';
        const type = 'Klaida ';
        if(!values.repeatVisitation){
            values.repeatVisitation = false;
        }
        if(!values.compensated) {
            values.compensated = false;
        }
        if(!values.notes) {
            values.notes = 'Nėra pastabų';
        }
        this.props.onCreateUser(url, values, token, formName, type);
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    filterHandler = (event, values) => {
        const token = this.props.tokenRedux;
        const url = this.diagnosisUrl;
        const pageSize = '3';
        if (values.length > 1) {
            this.props.onFetchDiagnoses(url, token, pageSize, values);
        } else if (values.length === 0) {
            this.props.onFetchDiagnoses(url, token, pageSize, values);
        }
    };

    render() {

        const myPatient = typeof this.props.match.params.id !== 'undefined';
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        const date = moment(new Date()).format("YYYY-MM-DD HH:mm");
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            const createdVisit = this.props.createdUserDataRedux;
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedVisit
                    image={this.state.image}
                    firstName={createdVisit.firstName}
                    lastName={createdVisit.lastName}
                    pid={createdVisit.patientPid}
                    visitDate={date}
                    myPatient={myPatient}
                    compensated={createdVisit.compensated}
                    repeatVisitation={createdVisit.repeatVisitation}
                    diagnosisTitle={createdVisit.diagnosisTitle}
                    diagnosisDescription={createdVisit.id.diagnosisDescription}
                    visitLength={createdVisit.appointmentLength}
                    notes={createdVisit.notes}
                    hide={this.hideModal}
                    back={this.props.history.goBack}/>
            </Modal>
        }

        let diagnoses = null;
        let diagnosisTitles = null;
        if (this.props.diagnosesListRedux) {
            diagnoses = this.props.diagnosesListRedux;
            diagnosisTitles = diagnoses.map(diagnosis => diagnosis.title)
        }

        return (
            <Aux>
                {createdUser}
                <MedicalHistoryRecordForm
                    diagnosisChange={this.filterHandler}
                    diagnosis={diagnosisTitles}
                    onSubmit={this.medicalHistoryRecordCreationHandler}
                    back={this.props.history.goBack}
                    myPatient={myPatient}>
                    {errorMessage}
                </MedicalHistoryRecordForm>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        userRedux: state.fetch.userList,
        authDoctor: state.auth.authUser,
        diagnosesListRedux: state.fetchOther.otherList,
        notMinePatient: state.getUser.userById
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onLoad: values => dispatch(initialize('MedicalHistoryRecordForm', values)),
        onFetchDiagnoses: (url, token, pageSize, searchParams) => dispatch(actions.fetchOthers(url, token, pageSize, searchParams)),
        regFieldPatientPid: () => dispatch(registerField('MedicalHistoryRecordForm', 'patientPid', 'Field')),
        regFieldFirstName: () => dispatch(registerField('MedicalHistoryRecordForm', 'firstName', 'Field')),
        regFieldLastName: () => dispatch(registerField('MedicalHistoryRecordForm', 'lastName', 'Field')),
        regFieldDiagnosisTitle: () => dispatch(registerField('MedicalHistoryRecordForm', 'diagnosisTitle', 'Field')),
        regFieldAppointmentLength: () => dispatch(registerField('MedicalHistoryRecordForm', 'appointmentLength', 'Field')),
        regFieldNotes: () => dispatch(registerField('MedicalHistoryRecordForm', 'notes', 'Field'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MedicalHistoryRecord);
