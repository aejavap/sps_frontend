import React, {Component} from 'react';
import PrescriptionForm from '../../../../components/Form/Forms/PrescriptionForm';
import Aux from "../../../../hoc/Aux";
import {initialize, registerField} from "redux-form";
import * as actions from "../../../../store/actions/dispatchActions";
import {connect} from "react-redux";
import CreatedPrescription from "../../../../components/UI/Modal/CreatedPrescription/CreatedPrescription";
import Modal from "../../../../components/UI/Modal/Modal";
import icon from '../../../../assets/usericon-guy.png';
import moment from "moment/moment";

class Prescription extends Component {
    state = {
        isCreated: false,
        image: icon,
    };

    componentWillMount() {
        const url = 'api/ingredients/search/by-title';
        const token = this.props.tokenRedux;
        this.props.onFetchIngredients(url, token, 3, 'A');
        this.props.onInitCreation();
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        this.props.regFieldPatientPid();
        this.props.regFieldFirstName();
        this.props.regFieldLastName();
        this.props.regFieldDosageNotes();
        this.props.regFieldIngredientName();
        this.props.regFieldIngredientPerDose();
        this.props.regFieldIngredientUnits();
        this.props.regFieldValidUntil();
        this.props.regFieldUnlimitedValidity();
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined' && this.props.userRedux && this.props.authDoctor) {
            const values = this.props.userRedux[number];
            const doctorId = this.props.authDoctor.id;
            const data = {...values, patientPid: values.pid, doctorId: doctorId};
            this.props.onLoad(data);
        } else if (typeof number !== 'undefined' && this.props.notMinePatient && this.props.authDoctor) {
            const patient = this.props.notMinePatient;
            const data = {...patient, patientPid: patient.pid};
            this.props.onLoad(data);
        } else this.props.history.replace('/doctor/newprescription')
    }

    prescriptionCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'api/doctor/new/prescription';
        const token = this.props.tokenRedux;
        const formName = 'PrescriptionForm';
        const type = 'kazkokia klaida';
        if (!values.dosageNotes) {
            values.dosageNotes = 'Nėra';
        }
        if (values.unlimitedValidity) {
            values.validUntil = moment("2999-12-31 00:00", "YYYY-MM-DD HH:mm").format("YYYY-MM-DD HH:mm");
        } else if (values.validUntil) {
            values.validUntil = moment(values.validUntil).format("YYYY-MM-DD HH:mm");
        }
        this.props.onCreateUser(url, values, token, formName, type);
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    filterHandler = (event, values) => {
        const token = this.props.tokenRedux;
        const url = 'api/ingredients/search/by-title';
        const pageSize = '3';
        if (values.length > 2) {
            this.props.onFetchIngredients(url, token, pageSize, values);
        } else if (values.length === 0) {
            this.props.onFetchIngredients(url, token, pageSize, values);
        }
    };

    render() {

        const myPatient = typeof this.props.match.params.id !== 'undefined';
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux) {
            const createdPrescription = this.props.createdUserDataRedux;
            let validUntil = new Date(createdPrescription.validUntil).toLocaleDateString("lt-LT");
            if (validUntil === '2999-12-31') {
                validUntil = 'Neterminuotas'
            }
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedPrescription
                    image={this.state.image}
                    firstName={createdPrescription.firstName}
                    lastName={createdPrescription.lastName}
                    pid={createdPrescription.patientPid}
                    activeIngredient={createdPrescription.activeIngredientTitle}
                    activeIngredientPerDose={createdPrescription.activeIngredientPerDose}
                    activeIngredientUnits={createdPrescription.activeIngredientUnits}
                    myPatient={myPatient}
                    validUntil={validUntil}
                    prescribedOn={new Date().toLocaleDateString("lt-LT")}
                    dosageNotes={createdPrescription.dosageNotes}
                    hide={this.hideModal}
                    back={this.props.history.goBack}/>
            </Modal>
        }

        let ingredients = null;
        let ingredientTitles = null;
        if (this.props.ingredientsListRedux) {
            ingredients = this.props.ingredientsListRedux;
            ingredientTitles = ingredients.map(ingredient => ingredient.title)
        }
        return (
            <Aux>
                {createdUser}
                <PrescriptionForm
                    ingredientChange={this.filterHandler}
                    onSubmit={this.prescriptionCreationHandler}
                    back={this.props.history.goBack}
                    myPatient={myPatient}
                    ingredient={ingredientTitles}>
                    {errorMessage}
                </PrescriptionForm>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        userRedux: state.fetch.userList,
        authDoctor: state.auth.authUser,
        ingredientsListRedux: state.fetchOther.otherList,
        notMinePatient: state.getUser.userById
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onLoad: values => dispatch(initialize('PrescriptionForm', values)),
        onFetchIngredients: (url, token, pageSize, searchParams) => dispatch(actions.fetchOthers(url, token, pageSize, searchParams)),
        regFieldPatientPid: () => dispatch(registerField('PrescriptionForm', 'patientPid', 'Field')),
        regFieldFirstName: () => dispatch(registerField('PrescriptionForm', 'firstName', 'Field')),
        regFieldLastName: () => dispatch(registerField('PrescriptionForm', 'lastName', 'Field')),
        regFieldIngredientName: () => dispatch(registerField('PrescriptionForm', 'activeIngredientTitle', 'Field')),
        regFieldIngredientPerDose: () => dispatch(registerField('PrescriptionForm', 'activeIngredientPerDose', 'Field')),
        regFieldIngredientUnits: () => dispatch(registerField('PrescriptionForm', 'activeIngredientUnits', 'Field')),
        regFieldValidUntil: () => dispatch(registerField('PrescriptionForm', 'validUntil', 'Field')),
        regFieldUnlimitedValidity: () => dispatch(registerField('PrescriptionForm', 'unlimitedValidity', 'Field')),
        regFieldDosageNotes: () => dispatch(registerField('PrescriptionForm', 'dosageNotes', 'Field'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Prescription);
