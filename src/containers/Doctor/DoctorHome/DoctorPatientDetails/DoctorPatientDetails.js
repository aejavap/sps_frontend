import React, {Component} from 'react';
import {Link, Redirect} from "react-router-dom";
import Aux from "../../../../hoc/Aux";
import UserCard from '../../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../../assets/usericon-guy.png';
import {Button} from 'react-bootstrap';
import MedicalHistoryList from "../../../Lists/MedicalHistoryList/MedicalHistoryList";
import PrescriptionList from "../../../Lists/PrescriptionList/PrescriptionList";
import {connect} from "react-redux";

class DoctorPatientDetails extends Component {

    state = {
        image: usericon,
    };

    render() {
        let userCard = <Redirect to='/doctor/home'/>;
        if (this.props.userRedux.length > 0) {
            const patient = this.props.match.params.pid;
            const userIndex = this.props.userRedux.findIndex(x => x.pid.toString() === patient);
            const myPatient = this.props.userRedux[userIndex];
            const buttonStyle = {width: '150px'};
            userCard = (<div>
                <UserCard
                    image={this.state.image}
                    firstName={myPatient.firstName}
                    lastName={myPatient.lastName}
                    pid={myPatient.pid}>
                    <p>{myPatient.dob}</p>
                    <p><Link to={"/doctor/newrecord/" + userIndex}>
                        <Button bsStyle="info" id='detailsNewRecord' style={buttonStyle}>
                            Naujas ligos įrašas
                        </Button>
                    </Link></p>
                    <p><Link to={"/doctor/newprescription/" + userIndex}>
                        <Button bsStyle="info" id='detailsNewPrescription' style={buttonStyle}>
                            Naujas receptas
                        </Button>
                    </Link></p>
                </UserCard>
                <h3>Ligos istorija</h3>
                <MedicalHistoryList/>
                <h3>Išrašyti receptai</h3>
                <PrescriptionList/>
            </div>)
        }

        return (
            <Aux>
                {userCard}
            </Aux>

        );
    }
}

const mapStateToProps = state => {
    return {
        userRedux: state.fetch.userList,
    }
};

export default connect(mapStateToProps)(DoctorPatientDetails);
