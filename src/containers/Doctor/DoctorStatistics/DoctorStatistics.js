import React, { Component } from 'react';
import Aux from '../../../../hoc/Aux';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';
import axios from 'axios';

import VisitTime from "./VisitTime";
import VisitNum from "./VisitNum";
import DateRange from "../../../../components/Form/Forms/DateRange"

class DoctorStatistics extends Component {
    state = {
        labels: [],
        visitData: [],
        timeData: []
    }

    static defaultProps = {
        visitNumUrl: '/api/doctor/stats/records/?startDate=2018-02-14&endDate=2018-03-13',
        visitTimeUrl: '/api/doctor/stats/records/visit-time?startDate=2018-02-14&endDate=2018-03-13'

    };
    componentDidMount() {
        const currentDate = moment().format('YYYY-MM-DD');
        const visitUrl = this.props.visitNumUrl;
        const visitTimeUrl = this.props.visitTimeUrl;
        axios.get(visitUrl)
            .then(response => {
                this.setState({ visitdata: Object.values(response.data) });
                console.log("statistics data received", response);
                return axios.get(visitTimeUrl);
            }).then(response => {
                this.setState({ timedata: Object.values(response.data) });
                console.log("statistics data received", response);
            })
            .catch(error => {
                console.log('statistics receive error ', error);
            })
        // let labels = Object.keys(response.data);
        // let data = Object.values(response.data);
        let dates = [];

        let startDate = moment('2017-02-14').startOf('day');
        let lastDate = moment('2018-03-13').startOf('day');

        while (startDate.add(1, 'days').diff(lastDate) < 0) {
            console.log(startDate.toDate());
            dates.push(startDate.clone().toDate());
        }

        this.setState({ labels: dates });
    }


    render() {

        const data = {
            labels: this.state.labels,
            datasets: [{
                label: 'Vizitų trukmė',
                type: 'line',
                data: this.state.timeData,
                fill: false,
                borderColor: '#EC932F',
                backgroundColor: '#EC932F',
                pointBorderColor: '#EC932F',
                pointBackgroundColor: '#EC932F',
                pointHoverBackgroundColor: '#EC932F',
                pointHoverBorderColor: '#EC932F',
                yAxisID: 'y-axis-2'
            }, {
                type: 'bar',
                label: 'Vizitų skaičius',
                data: this.state.visitData,
                fill: false,
                backgroundColor: '#71B37C',
                borderColor: '#71B37C',
                hoverBackgroundColor: '#71B37C',
                hoverBorderColor: '#71B37C',
                yAxisID: 'y-axis-1'
            }]
        };

        const options = {
            responsive: true,
            tooltips: {
                mode: 'label'
            },
            elements: {
                line: {
                    fill: false
                }
            },
            scales: {
                xAxes: [
                    {
                        display: true,
                        gridLines: {
                            display: false
                        },
                        labels: {
                            show: true
                        }
                    }
                ],
                yAxes: [
                    {
                        type: 'linear',
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                        gridLines: {
                            display: false
                        },
                        labels: {
                            show: true
                        }
                    },
                    {
                        type: 'linear',
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',
                        gridLines: {
                            display: false
                        },
                        labels: {
                            show: true
                        }
                    }
                ]
            }
        };

        return (
            <Aux>
                <div>
                    <h3>Statistika</h3>
                    <Bar
                        data={data}
                        options={options}
                    />
                </div>
            </Aux>
        );
    }
}
export default DoctorStatistics;