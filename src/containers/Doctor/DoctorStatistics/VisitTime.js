import React, { Component } from 'react';
import axios from 'axios';
import {Table} from "react-bootstrap";
import Aux from '../../../../hoc/Aux';

class VisitTime extends Component {
    state = {
        labels: [],
        data: []
    };

    static defaultProps = {
       visitTimeUrl:'/api/doctor/stats/records/visit-time',
    //    ?startDate=2017-03-14&endDate=2018-03-13

    };
    componentDidMount() {
        const url = this.props.visitTimeUrl 
        // + "?startDate=" + "&endDate=" + currentDate;
        console.log(url)
        axios.get(url)
            .then(response => {
            this.setState({labels: Object.keys(response.data)});
            this.setState({data: Object.values(response.data)});
            console.log("statistics data received", response);
            // console.log("labels", labels, "data", data);
            })
            .catch(error => {
                console.log('statistics receive error ', error);
            })
            // let labels = Object.keys(response.data);
            // let data = Object.values(response.data);
            
    }
render() {

    return (
        <Aux>
            <h3>Vizitų trukmė</h3>
        </Aux>
    );
}
}
export default VisitTime;