import React, { Component } from 'react';
import axios from 'axios';
import { Table } from "react-bootstrap";
import moment from 'moment';
import Aux from '../../../../hoc/Aux';

class VisitNum extends Component {
    state = {
        labels: [],
        data: []
    };

    static defaultProps = {
        visitNumUrl: '/api/doctor/stats/records/?startDate=2018-02-14&endDate=2018-03-13'

    };
    componentDidMount() {
        const currentDate = moment().format('YYYY-MM-DD');
        const url = this.props.visitNumUrl;
        axios.get(url)
            .then(response => {
                this.setState({ labels: Object.keys(response.data) });
                this.setState({ data: Object.values(response.data) });
                console.log("statistics data received", response);
                // console.log("labels", labels, "data", data);
            })
            .catch(error => {
                console.log('statistics receive error ', error);
            })
        // let labels = Object.keys(response.data);
        // let data = Object.values(response.data);

    }
    render() {
        
        return (
            <Aux>
                <h3>Vizitų skaičius</h3>
            </Aux>
        );
    }
}
export default VisitNum;