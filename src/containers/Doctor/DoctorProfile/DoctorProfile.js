import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import Aux from '../../../hoc/Aux';
import UserCard from '../../../components/DetailCards/UserCard/UserCard';
import usericon from '../../../assets/usericon.png';
import {connect} from "react-redux";
import AccountChange from "../../Administration/UserAccount/AccountChange";
import Spinner from "../../../components/UI/Spinner/Spinner";
import {Redirect} from "react-router-dom";

class DoctorProfile extends Component {
    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: 0,
            specializationTitle: '',
        },
        showPasswordForm: false,
        showUsernameForm: false,
        loading: false,
        error: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = {...this.props.userDetailsRedux, image: usericon};
            this.setState({user: authUser})
        }
    }

    changePasswordHandler = () => this.setState({
        showPasswordForm: !this.state.showPasswordForm,
        showUsernameForm: false
    });
    changeUsernameHandler = () => this.setState({
        showUsernameForm: !this.state.showUsernameForm,
        showPasswordForm: false
    });

    render() {

        const buttonStyle = {width: '150px'};
        let changePasswordForm = null;
        if (this.state.showPasswordForm) {
            changePasswordForm = <AccountChange back={this.changePasswordHandler} typePassword/>
        } else if (this.state.showUsernameForm) {
            changePasswordForm = <AccountChange back={this.changeUsernameHandler}/>
        }
        let userCard = null;
        if (!this.props.userDetailsRedux) {
            userCard = <Redirect to='/doctor/home'/>
        } else if ((this.state.loading || this.props.loadingRedux) && !this.state.error) {
            userCard = <Spinner/>
        } else if (this.state.error || this.props.errorRedux) {
            userCard = <h3>Tinklo klaida</h3>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                <span id='doctorProfileSpecialization'><strong>{this.state.user.specializationTitle}</strong></span>
                <p>
                    <Button id='adminChangePasswordButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
                <p>
                    <Button id='adminChangeUsernameButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changeUsernameHandler}>
                        Keisti vartotojo vardą
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
                {/* <div>DoctorStatistics</div> */}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error
    }
};

export default connect(mapStateToProps)(DoctorProfile);
