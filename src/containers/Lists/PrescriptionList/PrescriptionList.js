import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import * as actions from '../../../store/actions/dispatchActions';
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import axios from 'axios';
import moment from 'moment';

import Modal from "../../../components/UI/Modal/Modal";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Pagination from "../../../components/UI/Pagination/Pagination";
import PrescriptionListComp from "../../../components/Lists/Prescription/PrescriptionListComp"
import PrescriptionDetails from '../../../components/UI/Modal/Details/PrescriptionDetails';
import PrescriptionFillList from '../../../components/Lists/PrescriptionFillList/PrescriptionFillList';
import {role} from "../../../shared/utility";

class PrescriptionList extends Component {
    state = {
        error: null,
        showPrescription: false,
        prescriptionId: null,
        pageSize: '5',
        prescriptionFillList: [],
    };

    static defaultProps = {
        defaultPatientUrl: 'api/patient/prescription/all',
        defaultDoctorUrl: 'api/doctor/patient/'
    };

    componentDidMount() {
        const pageSize = this.state.pageSize;
        const patientPid = this.props.match.params.pid;
        const token = this.props.tokenRedux;
        const userType = role(token);
        let url = null;
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/prescription/all';
            return this.props.onFetchPrescriptions(url, token, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            return this.props.onFetchPrescriptions(url, token, pageSize);
        }
    }

    whatURL = pageNumber => {
        const pageSize = this.state.pageSize;
        const patientPid = this.props.match.params.pid;
        const token = this.props.tokenRedux;
        const userType = role(token);
        let url = null;
        if (userType === 'doctor') {
            url = this.props.defaultDoctorUrl + patientPid + '/prescription/all';
            return this.props.onFetchPrescriptionsChangePage(url, token, pageNumber, pageSize);
        } else if (userType === 'patient') {
            url = this.props.defaultPatientUrl;
            return this.props.onFetchPrescriptionsChangePage(url, token, pageNumber, pageSize);
        }
    };

    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.whatURL(pageNumber);
        }
    };

    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if (pageNumber >= 0) {
            this.whatURL(pageNumber);
        }
    };

    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux) {
            this.whatURL(pageNumber);
        }
    };

    firstPage = () => {
        if (this.props.pageNumberRedux > 0) {
            this.whatURL(0)
        }
    };

    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0) {
            this.whatURL(pageNumber);
        }
    };

    prescriptionDetails = index => {
        this.setState({showPrescription: !this.state.showPrescription, prescriptionId: index});
        const prescriptionFillId = {
            prescriptionId: this.props.prescriptionListRedux[index].prescriptionId
        };
        if (this.props.tokenRedux) {
            const token = this.props.tokenRedux;
            const userType = role(token);
            const fillUrl = `api/${userType}/prescriptionFills/${prescriptionFillId.prescriptionId}/search/`;
            axios.get(fillUrl, {headers: {'Authorization': `Bearer ${token}`}})
                .then(response => {
                    this.setState({prescriptionFillList: response.data.content});
                })
                .catch(error => {
                })
        }
    };

    render() {
        const currentDate = moment().format('YYYY-MM-DD');
        let prescriptionList = <Spinner/>;

        if (this.props.loadingRedux) {
            prescriptionList = <Spinner/>;
        }
        if (this.props.errorRedux) {
            prescriptionList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            prescriptionList = <Redirect to='/'/>
        }

        else {
            let pagination = null;
            if (this.props.totalPagesRedux > 1) {
                 pagination =
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />;
            }
            prescriptionList = (
                <div  style={{width: '70%', margin: 'auto'}}>
                    <PrescriptionListComp
                        prescriptionList={this.props.prescriptionListRedux}
                        key={this.props.prescriptionListRedux.prescriptionId}
                        validUntil={this.props.prescriptionListRedux.validUntil}
                        prescribedOn={this.props.prescriptionListRedux.prescriptionDate}
                        activeIngredientTitle={this.props.prescriptionListRedux.activeIngredientTitle}
                        fillsNo={this.props.prescriptionListRedux.fillsNo}
                        prescriptionAction={this.prescriptionDetails}
                        canSeePatient={true}
                        isPharmacist={false}
                        currentDate={currentDate}
                        buttonTitle="Detaliau"
                        prescriptionFills={
                            <PrescriptionFillList
                                prescriptionFillList={this.state.prescriptionFillList}
                                key={this.state.prescriptionFillList.date}
                                date={this.state.prescriptionFillList.date}
                                phFirstName={this.state.prescriptionFillList.phFirstName}
                                phLastName={this.state.prescriptionFillList.phLastName}
                            />}
                    />
                    {pagination}
                </div>
            );
        }

        let prescriptionDetails = null;
        let validUntil = null;

        if (this.state.showPrescription) {
            const index = this.state.prescriptionId;
            const prescription = this.props.prescriptionListRedux[index];
            if (prescription.validUntil === "2999-12-31") {
                validUntil = "Neterminuotas";
            } else {
                validUntil = prescription.validUntil;
            }
            prescriptionDetails = <Modal show={this.state.showPrescription} hide={this.prescriptionDetails}>
                <PrescriptionDetails
                    prescribedOn={prescription.prescriptionDate}
                    validUntil={validUntil}
                    activeIngredientTitle={prescription.activeIngredientTitle}
                    activeIngredientPerDose={prescription.activeIngredientPerDose}
                    activeIngredientUnits={prescription.activeIngredientUnits}
                    doctorFirstName={prescription.doctorFirstName}
                    doctorLastName={prescription.doctorLastName}
                    dosageNotes={prescription.dosageNotes}
                    fillsNo={prescription.fillsNo}
                    hide={this.prescriptionDetails}
                    back={this.prescriptionDetails}
                    prescriptionFills={<PrescriptionFillList
                        prescriptionFillList={this.state.prescriptionFillList}
                        key={this.state.prescriptionFillList.date}
                        date={this.state.prescriptionFillList.date}
                        phFirstName={this.state.prescriptionFillList.phFirstName}
                        phLastName={this.state.prescriptionFillList.phLastName}
                    />}
                />
            </Modal>
        }
        return (
            <div className="container">
                {prescriptionList}
                {prescriptionDetails}
            </div>
        );
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        prescriptionListRedux: state.fetchPrescriptions.prescriptionList,
        loadingRedux: state.fetchPrescriptions.loading,
        errorRedux: state.fetchPrescriptions.error,
        tokenRedux: state.auth.accessToken,
        totalPagesRedux: state.fetchPrescriptions.totalPages,
        pageNumberRedux: state.fetchPrescriptions.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchPrescriptions: (url, token, pageSize) => dispatch(actions.fetchPrescriptions(url, token, pageSize)),
        onFetchPrescriptionsChangePage: (url, token, pageNumber, pageSize) => dispatch(actions.changePageFetchPrescriptions(url, token, pageNumber, pageSize))

    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrescriptionList));
