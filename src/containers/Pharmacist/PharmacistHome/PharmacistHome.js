import React, { Component } from 'react';
import moment from 'moment';
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import * as actions from '../../../store/actions/dispatchActions';
import Aux from "../../../hoc/Aux";
import Modal from "../../../components/UI/Modal/Modal";
import PrescriptionSearchBar from '../../../components/UI/SearchBar/PrescriptionSearchBar';
import PrescriptionList from '../../../components/Lists/Prescription/PrescriptionListComp';
import PrescriptionFillList from '../../../components/Lists/PrescriptionFillList/PrescriptionFillList';
import PrescriptionDetails from '../../../components/UI/Modal/Details/PrescriptionDetails'
import CreatedPrescriptionFill from '../../../components/UI/Modal/CreatedPrescriptionFill/CreatedPrescriptionFill';

class PharmacistHome extends Component {
    state = {
        searchPid: '',
        pageSize: '10',
        error: null,
        prescriptionId: null,
        prescriptionFillDate: null,
        prescriptionFillList: [],
        showPrescription: false,
        prescriptionFillCreated: false,
        isSearched: false,
    };

    static defaultProps = {
        defaultUrl: 'api/pharmacist/prescription/patient/'
    };

    componentWillMount() {
        this.props.onFetchPrescriptionsInit();
    }

    searchHandler = (value) => {
        this.setState({ isSearched: true });
        const searchPid = value.pid;
        const url = this.props.defaultUrl + searchPid;
        const token = this.props.tokenRedux;
        const pageSize = this.state.pageSize;
        this.props.onFetchPrescriptions(url, token, pageSize);
    };

    prescriptionDetails = (index) => {
        this.setState({ showPrescription: !this.state.showPrescription, prescriptionId: index });
        const prescriptionFillId = {
            prescriptionId: this.props.prescriptionListRedux[index].prescriptionId
        };
        let fillUrl = 'api/pharmacist/prescription-fill/' + prescriptionFillId.prescriptionId;

        const token = this.props.tokenRedux;
        if (token) {
            axios.get(fillUrl, { headers: { 'Authorization': 'Bearer ' + token } })
                .then(response => {
                    this.setState({ prescriptionFillList: response.data.content });
                })
                .catch(error => {
                    console.log('prescriptionFillList receive error ', error);
                })
        }
    };

    submitPrescriptionFill = (index) => {
        this.setState({ prescriptionFillCreated: !this.state.prescriptionFillCreated, showPrescription: false, prescriptionId: index });
        const prescriptionFillId = {
            prescriptionId: this.props.prescriptionListRedux[index].prescriptionId
        };
        let postUrl = 'api/pharmacist/new/prescription-fill/';

        const token = this.props.tokenRedux;
        if (token) {
            axios.post(postUrl, prescriptionFillId, { headers: { 'Authorization': `Bearer ${token}` } })
                .then(response => {
                })
                .catch(error => {
                    console.log('prescriptionFill send error ', error);
                })
        }
    };

    hideDetailsModal = () => {
        this.setState({ showPrescription: false })
    };

    hideFillModal = () => {
        this.setState({ prescriptionFillCreated: false })

    };

    render() {
        const searchPid = this.state.searchPid;
        let prescriptionList = null;
        let errorMessage = null;
        const currentDate = moment().format('YYYY-MM-DD');
        let tempList = [];
        for (let i = 0; i < this.props.prescriptionListRedux.length; i++) {
            if (this.props.prescriptionListRedux[i].validUntil >= currentDate) {
                tempList.push(this.props.prescriptionListRedux[i]);
            }
        }

        if (this.props.loadingRedux) {
            prescriptionList = null;
        }
        if (this.props.errorRedux) {
            errorMessage = <h4>Klaida! Bandykite dar kartą.</h4>
        }

        if (this.props.prescriptionListRedux.length === 0 && this.state.isSearched) {
            errorMessage = <h4>Vartotojo tokiu asmens kodu nėra arba jam nėra išrašyta galiojančių receptų.</h4>
        }

        if (!this.props.tokenRedux) {
            prescriptionList = <Redirect to='/' />
        }
        else if (tempList.length >= 0) {
            prescriptionList = (
                <div style={{width: '70%', margin: 'auto'}}>
                    <PrescriptionList
                        prescriptionList={tempList}
                        key={tempList.prescriptionId}
                        validUntil={tempList.validUntil}
                        prescribedOn={tempList.prescriptionDate}
                        activeIngredient={tempList.activeIngredient}
                        prescriptionAction={this.prescriptionDetails}
                        canSeePatient={false}
                        // isPharmacist={true}
                        buttonTitle="Detaliau">
                    </PrescriptionList>
                </div>
            );
        }

        let prescriptionDetails = null;
        let validUntil = null;
        if (this.state.showPrescription) {
            const index = this.state.prescriptionId;
            const prescription = this.props.prescriptionListRedux[index];
            if (prescription.validUntil === "2999-12-31") {
                validUntil = "Neterminuotas";
            } else {
                validUntil = prescription.validUntil;
            }
            prescriptionDetails =
                <Modal show={this.state.showPrescription} hide={this.hideDetailsModal}>
                    <PrescriptionDetails
                        prescribedOn={prescription.prescriptionDate}
                        validUntil={validUntil}
                        activeIngredientTitle={prescription.activeIngredientTitle}
                        activeIngredientPerDose={prescription.activeIngredientPerDose}
                        activeIngredientUnits={prescription.activeIngredientUnits}
                        // does not update
                        fillsNo={prescription.fillsNo}
                        doctorFirstName={prescription.doctorFirstName}
                        doctorLastName={prescription.doctorLastName}
                        dosageNotes={prescription.dosageNotes}
                        hide={this.hideDetailsModal}
                        // back={this.prescriptionFill}
                        prescriptionFills={
                            <PrescriptionFillList
                                prescriptionFillList={this.state.prescriptionFillList}
                                key={this.state.prescriptionFillList.date}
                                date={this.state.prescriptionFillList.date}
                                phFirstName={this.state.prescriptionFillList.phFirstName}
                                phLastName={this.state.prescriptionFillList.phLastName}
                            />}
                    >
                        <button onClick={() => this.submitPrescriptionFill(index)} type="button" className="btn btn-secondary" data-dismiss="modal">Panaudoti</button>
                    </PrescriptionDetails>
                </Modal>
        }
        let prescriptionFillRecord = null;
        if (this.state.prescriptionFillCreated) {
            prescriptionFillRecord =
                <Modal show={this.state.prescriptionFillCreated} hide={this.hideFillModal}>
                    <CreatedPrescriptionFill
                        hide={this.hideFillModal}
                    />
                </Modal>
        }
        return (
            <Aux>
                <h3>Receptų paieška</h3>
                <div>
                    <PrescriptionSearchBar value={searchPid} onSubmit={this.searchHandler}>  </PrescriptionSearchBar>
                </div>
                <br />
                <div>
                    {prescriptionList}
                    {prescriptionDetails}
                    {prescriptionFillRecord}
                    {errorMessage}
                </div>
            </Aux>
        );
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        prescriptionListRedux: state.fetchPrescriptions.prescriptionList,
        loadingRedux: state.fetchPrescriptions.loading,
        errorRedux: state.fetchPrescriptions.error,
        tokenRedux: state.auth.accessToken,
        totalPagesRedux: state.fetchPrescriptions.totalPages,
        pageNumberRedux: state.fetchPrescriptions.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchPrescriptionsInit: () => dispatch(actions.fetchPrescriptionsInit()),
        onFetchPrescriptions: (url, token, pageSize) => dispatch(actions.fetchPrescriptions(url, token, pageSize)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PharmacistHome);

