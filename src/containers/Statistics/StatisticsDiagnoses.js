import React, { Component } from 'react';
import axios from 'axios';
import { HorizontalBar } from 'react-chartjs-2';
import moment from 'moment';
import Aux from '../../hoc/Aux';
import {connect} from "react-redux";

class StatisticsDiagnoses extends Component {
    state = {
        labels: [],
        data: [],
    };

    static defaultProps = {
        defaultUrl: 'top-diagnoses/pc/?startDate=2018-02-14&endDate=2018-03-13'
    };

    componentDidMount() {
        const currentDate = moment().format('YYYY-MM-DD');
        const url = this.props.defaultUrl + currentDate;
        const token = this.props.tokenRedux;
        axios.get(url, {headers: {'Authorization': `Bearer ${token}`}})
            .then(response => {
                this.setState({ labels: Object.keys(response.data).slice(0, 10) });
                this.setState({ data: Object.values(response.data).slice(0, 10) });
                console.log("statistics data received", response);

            })
            .catch(error => {
                console.log('statistics receive error ', error);
            })
    }


    render() {
        console.log("labels", this.state.labels, "data", this.state.data);
        const StatisticsBar = {
            labels: this.state.labels,
            datasets: [
                {
                    label: 'Diagnozių dažnis (proc.)',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: this.state.data
                }
            ]
        };

        return (
            <Aux>
                <h3>Diagnozių statistika</h3>
                <br />
                <div>
                    <div>
                        <HorizontalBar data={StatisticsBar} />
                    </div>
                </div>
            </Aux>
        );
    }
}
const mapStateToProps = state => {
    return {
        tokenRedux: state.auth.accessToken,
    }
};

export default connect(mapStateToProps)(StatisticsDiagnoses);
