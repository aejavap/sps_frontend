import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import StatisticsDiagnoses from "./StatisticsDiagnoses";
import StatisticsDrugs from "./StatisticsDrugs";

class Statistics extends Component {

    render() {

        return (
            <Aux>
                <StatisticsDiagnoses/>
                <StatisticsDrugs/>
            </Aux>
        );
    }
}
export default Statistics;
