import React, { Component } from 'react';
import axios from 'axios';
import { HorizontalBar } from 'react-chartjs-2';
import moment from 'moment';
import Aux from '../../hoc/Aux';
import {connect} from "react-redux";

class StatisticsDrugs extends Component {
    state = {
        labels: [],
        data: [],
        tableData: []
    };

    static defaultProps = {
        defaultUrl: 'top-ingredients/pc/?startDate=1999-01-01&endDate='
    };

    componentDidMount() {
        const currentDate = moment().format('YYYY-MM-DD');
        const url = this.props.defaultUrl + currentDate;
        axios.get(url)
            .then(response => {
                this.setState({ labels: Object.keys(response.data).slice(0, 10) });
                this.setState({ data: Object.values(response.data).slice(0, 10) });
                this.setState({ tableData: Object.entries(response.data).slice(0, 10) });
                console.log("statistics data received", response);
            })
            .catch(error => {
                console.log('statistics receive error ', error);
            })
    }


    render() {
        console.log("labels", this.state.labels, "data", this.state.data, "tableData", this.state.tableData)
        const StatisticsBar = {
            labels: this.state.labels,
            datasets: [
                {
                    label: 'Vaistų naudojimo dažnis (proc.)',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: this.state.data
                }
            ]
        };

        // let tableData = [];
        // let tableRow = [];
        // for (let i = 0; i <= this.state.tableData.length; i++) {
        //     tableRow.push(this.state.tableData[i]);
        //     tableData.push(tableRow);
        //     tableRow = [];
        // }
        return (
            <Aux>
                <h3>Vaistų naudojimo statistika</h3>
                <br />
                <div>
                    <div>
                        <HorizontalBar data={StatisticsBar} />
                    </div>
                </div>
            </Aux>
        );
    }
}
const mapStateToProps = state => {
    return {
        tokenRedux: state.auth.accessToken,
    }
};

export default connect(mapStateToProps)(StatisticsDrugs);
