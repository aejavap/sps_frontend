import React, {PureComponent} from 'react';
import './AdminUserListClass.css';
import UserList from "../../components/Lists/UserList/userList";
import Spinner from "../../components/UI/Spinner/Spinner";
import {connect} from "react-redux";
import * as actions from '../../store/actions/dispatchActions'
import Aux from "../../hoc/Aux";
import Pagination from "../../components/UI/Pagination/Pagination";
import {SelectType, SearchFilterByFirstName, SearchFilterByLastName, SearchFilterByPid, SelectPageSize} from "../../components/UI/SearchBar/SearchBar";
import {Redirect} from "react-router-dom";
import {toFirstUpperCaseToLowerCase, onlyNumbers} from '../../shared/utility';

class AdminUserList extends PureComponent {

    state = {
        userType: 'admin',
        filtered: {
            firstName: '',
            lastName: '',
            pid: 0,
        },
        pageSize: '10',
    };
    getUrl = 'api/admin/';
    registerUrl = '/admin/register/';

    static defaultProps = {
        defaultSearch: {
            firstName: '',
            lastName: '',
            pid: 0
        }
    };

    componentWillMount() {
        const token = this.props.tokenRedux;
        const url = `${this.getUrl}${this.state.userType}/search`;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        this.props.onFetchUsers(url, token, pageSize, searchParams);
    }

    editUser = index => this.props.history.push(`${this.registerUrl}${this.state.userType}/${this.props.userListRedux[index].id}`);

    userAccount = index => this.props.history.push(`/admin/register/patient/account/${this.props.userListRedux[index].id}`);

    showUserDetails = index => this.props.history.push(`/admin/userList/userDetails/${this.props.userListRedux[index].pid}`);

    changePage = pageNumber => {
        const token = this.props.tokenRedux;
        const url = `${this.getUrl}${this.state.userType}/search`;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        return this.props.onFetchUsersChangePage(url, token, pageNumber, pageSize, searchParams);
    };
    nextPage = () => {
        const pageNumber = this.props.pageNumberRedux + 1;
        if (pageNumber < this.props.totalPagesRedux) {
            this.changePage(pageNumber);
        }
    };
    previousPage = () => {
        const pageNumber = this.props.pageNumberRedux - 1;
        if (pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };
    lastPage = () => {
        const pageNumber = this.props.totalPagesRedux - 1;
        if (pageNumber > this.props.pageNumberRedux) {
            this.changePage(pageNumber);
        }
    };
    firstPage = () => {
        if (this.props.pageNumberRedux > 0) {
            this.changePage(0);
        }
    };
    setPage = event => {
        const pageNumber = event.target.value - 1;
        if (pageNumber < this.props.totalPagesRedux && pageNumber >= 0) {
            this.changePage(pageNumber);
        }
    };

    userTypeHandler = (event, index, userType) => {
        this.setState({userType: userType});
        const token = this.props.tokenRedux;
        const pageSize = this.state.pageSize;
        const searchParams = this.state.filtered;
        const url = `${this.getUrl}${userType}/search`;
        if (searchParams.lastName.length < 3 && searchParams.pid.toString().length < 3 && searchParams.firstName.length < 3) {
            const searchParams = this.props.defaultSearch;
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        } else {
            this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
        }
    };

    filterHandler = (event, element) => {
        const searchParams = {...this.state.filtered};
        searchParams[element] = event.target.value;
        if (!searchParams.pid) {
            searchParams.pid = 0;
            this.setState({filtered: searchParams});
        } else if (!/^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.pid.toString().length > 1) {
                const sub = searchParams.pid.toString().length - 1;
                searchParams.pid = searchParams.pid.toString().substring(0, sub);
                this.setState({filtered: searchParams});
            } else {
                searchParams.pid = 0;
                this.setState({filtered: searchParams});
            }
        } else {
            this.setState({filtered: searchParams});
        }
        const token = this.props.tokenRedux;
        const url = `${this.getUrl}${this.state.userType}/search`;
        const pageSize = this.state.pageSize;
        if (searchParams.pid === 0 || /^\d+$/.test(searchParams.pid.toString())) {
            if (searchParams.lastName.length >= 3 || searchParams.pid.toString().length >= 3 || searchParams.firstName.length >= 3) {
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            } else if (searchParams.lastName.length === 0 && searchParams.pid.toString() === '0' && searchParams.firstName.length === 0) {
                const searchParams = this.props.defaultSearch;
                this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
            }
        }
    };

    pageSizeHandler = (event, index, pageSize) => {
        this.setState({pageSize: pageSize});
        const token = this.props.tokenRedux;
        const url = `${this.getUrl}${this.state.userType}/search`;
        let searchParams = this.state.filtered;
        this.props.onFetchUsersChangePage(url, token, 0, pageSize, searchParams);
    };

    render() {

        let userList = <Spinner/>;

        if (this.props.loadingRedux) {
            userList = <Spinner/>;
        }
        if (this.props.errorRedux) {
            userList = <h3>{this.props.errorRedux}</h3>
        }
        if (!this.props.tokenRedux) {
            userList = <Redirect to='/'/>
        }
        if (this.props.userListRedux) {
            let pid = onlyNumbers(this.state.filtered.pid);
            if (this.state.filtered.pid === 0) {
                pid = '';
            }
            const firstName = toFirstUpperCaseToLowerCase(this.state.filtered.firstName);
            const lastName = toFirstUpperCaseToLowerCase(this.state.filtered.lastName);
            const isPatient = this.state.userType === 'patient';

            let pagination = null;
            if (this.props.totalPagesRedux > 1) {
                pagination =
                    <Pagination
                        currentPage={this.props.pageNumberRedux + 1}
                        lastPage={this.props.totalPagesRedux}
                        first={this.firstPage}
                        previous={this.previousPage}
                        next={this.nextPage}
                        last={this.lastPage}
                        setPage={(event) => this.setPage(event)}
                    />
            }
            userList = (
                <div style={{width: '70%', margin: 'auto'}}>
                    <div id='AdminUserListSelectFilters' style={{textAlign: 'left'}}>
                        <SelectType
                            handleChange={this.userTypeHandler}
                            userType={this.state.userType}/>
                        <SelectPageSize
                            pageSize={this.state.pageSize}
                            handleChange={this.pageSizeHandler}/><br/>
                    </div>
                    <Aux>
                        <SearchFilterByPid
                            searchValue={pid}
                            handleChange={(event) => this.filterHandler(event, 'pid')}/>
                        <SearchFilterByFirstName
                            searchValue={firstName}
                            handleChange={(event) => this.filterHandler(event, 'firstName')}/>
                        <SearchFilterByLastName
                            searchValue={lastName}
                            handleChange={(event) => this.filterHandler(event, 'lastName')}/>
                    </Aux>
                    <UserList
                        userList={this.props.userListRedux}
                        pid={this.props.userListRedux.pid}
                        firstName={this.props.userListRedux.firstName}
                        lastName={this.props.userListRedux.lastName}
                        userEdit={this.editUser}
                        userDetails={this.showUserDetails}
                        userAccount={this.userAccount}
                        patient={isPatient}
                        hasAccount={this.props.userListRedux.hasAccount}
                    />
                    {pagination}
                </div>);
        }

        return (
            <div className="container">
                {userList}
            </div>
        )
    }
}

// Redux states
const mapStateToProps = state => {
    return {
        userListRedux: state.fetch.userList,
        loadingRedux: state.fetch.loading,
        errorRedux: state.fetch.error,
        tokenRedux: state.auth.accessToken,
        totalPagesRedux: state.fetch.totalPages,
        totalElementsRedux: state.fetch.totalElements,
        pageSizeRedux: state.fetch.pageSize,
        pageNumberRedux: state.fetch.pageNumber
    }
};

// Redux actions
const mapDispatchToProps = dispatch => {
    return {
        onFetchUsers: (url, token, pageSize, data) => dispatch(actions.fetchUsers(url, token, pageSize, data)),
        onFetchUsersChangePage: (url, token, pageNumber, pageSize, searchParams) => dispatch(actions.changePageFetchUsers(url, token, pageNumber, pageSize, searchParams)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminUserList);
