import React, {Component} from 'react';
import AccountChangeForm from "../../../components/Form/Forms/AccountChangeForm";
import axios from "axios/index";
import {connect} from "react-redux";
import {reset} from 'redux-form';
import Modal from "../../../components/UI/Modal/Modal";
import SuccessModal from '../../../components/UI/Modal/SuccessModal/SuccessModal';
import {withRouter} from "react-router-dom";
import * as actions from '../../../store/actions/dispatchActions'

class AccountChange extends Component {
    state = {
        error: null,
        showModal: false,
        changed: false
    };

    axiosPut = (data, password, errorMessage) => {
        const token = this.props.tokenRedux;
        axios.put("api/me/account/update", data, {headers: {'Authorization': `Bearer ${token}`}, params: {currentPasswordGiven: password}})
            .then(() => {
                this.props.onSubmitSuccess();
                this.setState({showModal: true, changed: true});
                if (errorMessage) {
                    setTimeout(() => this.props.onLogout(), 5000);
                }
            }).catch(error => {
            if (error.response && error.response.data.message){
                if (error.response.data.message === 'Failed authentication: password did not match.') {
                    const errorM = 'Nurodytas neteisingas dabartinis slaptažodis';
                    this.setState({error: errorM});
                } else {
                    this.setState({error: errorMessage});
                }
            } else {
                this.setState({error: 'Tinklo klaida'})
            }
        });
    };

    changePasswordHandler = values => {
        const currentPassword = values.password;
        const newPassword = {password: values.newPasswordConfirmation};
        const newUsername = {username: values.username};
        if (this.props.typePassword) {
            this.axiosPut(newPassword, currentPassword, null);
        } else {
            const error = 'Nurodytas vartotojo vardas jau užimtas';
            this.axiosPut(newUsername, currentPassword, error);
        }
    };

    modalHandler = () => this.setState({showModal: !this.state.showModal});

    render() {

        const modalHeader = 'Paskyros duomenų pakeitimas';
        const modalMessage = this.props.typePassword ? 'Jūsų slaptažodis sėkmingai pakeistas' :
            <div>
                <p>Jūsų vartotojo vardas sėkmingai pakeistas</p>
                <p><strong>Prisijunkite su nauju prisijungimo vardu.</strong></p>
            </div>;
        const successModal = <Modal show={this.state.showModal} hide={this.props.back}>
            <SuccessModal hide={this.props.back} modalHeader={modalHeader} modalMessage={modalMessage}/></Modal>;
        let errorMessage = null;
        if (this.state.error) {
            errorMessage = <span>{this.state.error}</span>;
        }

        return (
            <div>
                <AccountChangeForm changePassword={this.props.typePassword} onSubmit={this.changePasswordHandler} back={this.props.back}>
                    {errorMessage}
                </AccountChangeForm>
                {successModal}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authUserDataRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onSubmitSuccess: () => dispatch(reset('AccountChangeForm')),
        onLogout: () => dispatch(actions.authLogout())
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountChange));
