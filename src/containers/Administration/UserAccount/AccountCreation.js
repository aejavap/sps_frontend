import React, {Component} from 'react';
import AccountForm from '../../../components/Form/Forms/AccountForm'
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions'
import Modal from "../../../components/UI/Modal/Modal";
import CreatedAccount from "../../../components/UI/Modal/CreatedAccount/CreatedAccount";
import icon from '../../../assets/usericon-guy.png';

class AccountCreation extends Component {

    state = {
        isCreated: false,
        image: icon
    };

    componentWillMount() {
        this.props.onFetchUserInit();
    }

    componentDidMount() {
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const url = `api/admin/patient/${number}`;
        this.props.onFetchUser(url, token);
    }

    adminCreationHandler = values => {
        this.setState({isCreated: true});
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const url = `api/admin/new/patient-account/${number}`;
        const form = 'AccountForm';
        const type = 'Vartotojas';
        const accountData = {username: values.username, password: values.password, id: values.id};
        if (typeof number !== 'undefined') {
            this.props.onCreateUser(url, accountData, token, form, type, null);
        }
    };

    hideModal = () => {
        this.setState({isCreated: false});
    };

    render() {
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let showPassword= false;
        if (typeof this.props.formValues.AccountForm !== 'undefined') {
            if (typeof this.props.formValues.AccountForm.values !== 'undefined') {
                const values = this.props.formValues.AccountForm.values;
                if(values.showPassword) {
                    showPassword = true
                }
            }
        }
        let createdUser = null;
        if (this.props.createdUserDataRedux && this.props.userDataRedux) {
            createdUser = <Modal
                show={this.state.isCreated}
                hide={this.hideModal}>
                <CreatedAccount
                    image={this.state.image}
                    firstName={this.props.userDataRedux.firstName}
                    lastName={this.props.userDataRedux.lastName}
                    pid={this.props.userDataRedux.pid}
                    hide={() => this.hideModal}
                    back={this.props.history.goBack}
                    username={this.props.createdUserDataRedux.username}/>
            </Modal>
        }
        return (
            <div>
                {createdUser}
                <AccountForm created={true} onSubmit={this.adminCreationHandler} back={this.props.history.goBack} showPassword={showPassword}>
                {errorMessage}
                </AccountForm>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        // loadingRedux: state.create.loading,
        createdUserDataRedux: state.create.userData,
        // loadingRedux: state.getUser.loading,
        userDataRedux: state.getUser.userById,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        formValues: state.form
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountCreation);
