import React, {Component} from 'react';
import Aux from "../../../hoc/Aux";
import {connect} from "react-redux";
import usericon from '../../../assets/usericon.png'
import UserCard from "../../../components/DetailCards/UserCard/UserCard";
import UserDetails from '../../../components/DetailCards/UserDetails/UserDetails';
import axios from 'axios';
import Spinner from "../../../components/UI/Spinner/Spinner";
import {userType, loggedUser} from '../../../shared/utility';
import {Button} from "react-bootstrap";
import Modal from "../../../components/UI/Modal/Modal";
import SuccessModal from "../../../components/UI/Modal/SuccessModal/SuccessModal";


class AdminUserDetails extends Component {
    state = {
        image: usericon,
        userAccount: null,
        error: null,
        showModal: false
    };
    activationURL = 'api/admin/account/';
    userAccountURL = 'api/admin/user/account/';

    getUser = () => {
        const userPid = this.props.match.params.pid;
        const userIndex = this.props.userRedux.findIndex(usr => usr.pid.toString() === userPid);
        return this.props.userRedux[userIndex];
    };

    componentDidMount() {
        if (typeof this.props.match.params.pid !== 'undefined' && this.props.userRedux && this.props.userRedux.length > 0) {
            const user = this.getUser();
            if ((user.hasAccount && user.hasAccount === true) || typeof user.hasAccount === 'undefined') {
                const token = this.props.tokenRedux;
                const url = `${this.userAccountURL}${user.id}`;
                axios.get(url, {headers: {'Authorization': `Bearer ${token}`}
                }).then(response => {
                    this.setState({userAccount: response.data});
                }).catch(error => this.setState({error: error.response.data.message}))
            }
        } else {
            this.props.history.replace('/admin/home')
        }
    }

    accountActivationHandler = () => {
        const token = this.props.tokenRedux;
        let enabled = this.state.userAccount.enabled;
        if (enabled === false || enabled === 'Ne') {
            enabled = 'true';
        } else {
            enabled = 'false';
        }
        const user = this.getUser();
        const url = `${this.activationURL}${user.id}/${enabled}`;
        axios.put(url, null, {headers: {'Authorization': `Bearer ${token}`}})
            .then(() => {
                this.componentDidMount();
                this.setState({showModal: true})
                // setTimeout(() => this.props.history.replace(this.props.match.params.pathname), 2000);
            }).catch(error => {
            this.setState({error: error});
        })
    };

    passwordResetHandler = () => {
        const user = this.getUser();
        const token = this.props.tokenRedux;
        const url = `${this.activationURL}${user.id}/reset-password`;
        axios.put(url, null, {headers: {'Authorization': `Bearer ${token}`}})
            .then(() => {
                this.setState({showModal: true});
            }).catch(error => {
            this.setState({error: error});
        })
    };

    userAccount = () => this.props.history.push(`/admin/register/patient/account/${this.getUser().id}`);
    modalHandler = () => this.setState({showModal: !this.state.showModal});

    render() {

        const buttonStyle = {width: '150px'};
        let userCard = <Spinner/>;
        let userDetails = null;
        let hasAccount = null;
        let activateAccButton = null;
        let addAccount = null;
        if (this.props.userRedux && this.props.userRedux.length > 0) {
            const user = this.getUser();
            let hasAcc = null;
            if (user.hasAccount && user.hasAccount === true) {
                hasAcc = 'Taip';
            } else if (user.hasAccount === false) {
                hasAcc = 'Ne';
            } else {
                hasAcc = 'Taip';
            }
            hasAccount = {...user, hasAccount: hasAcc};

            let userAccount = null;
            if (this.state.userAccount && this.state.userAccount.roles) {
                const userAcc = this.state.userAccount;
                const aktyvus = userAcc.enabled ? 'Taip' : 'Ne';
                const roles = loggedUser(userType(userAcc.roles));
                userAccount = {...userAcc, enabled: aktyvus, roles: roles};
                activateAccButton = <p>
                    <Button bsStyle={this.state.userAccount.enabled ? 'danger' : 'success'} style={buttonStyle}
                            onClick={this.accountActivationHandler}>
                        {this.state.userAccount.enabled ? 'Deaktyvuoti' : 'Aktyvuoti'}
                    </Button>
                </p>;
            } else {
                addAccount = <Button style={{margin: '30px'}} bsSize='large' bsStyle="success"
                                     onClick={this.userAccount}>Pridėti paskyrą</Button>
            }

            userDetails = <UserDetails user={hasAccount} userAccount={userAccount}/>;

            userCard = <UserCard
                image={this.state.image}
                firstName={user.firstName}
                lastName={user.lastName}
                pid={user.pid}>
                <p>{user.dob}</p>
                <p>
                    <Button onClick={this.passwordResetHandler} bsStyle="warning" style={buttonStyle}>
                        Atstatyti slaptažodį
                    </Button>
                </p>
                {activateAccButton}
            </UserCard>;
        }

        const modalHeader = 'Užklausa įvykdyta sėkmingai';
        // const modalMessageActivation = 'Vartotojo paskyra sėkmingai aktyvuota';
        // const modalMessageDeaktyvation = 'Vartotojas paskyra sėkmingai deaktyvuota';
        // const modalMessageReset = 'Vartotojo slaptažodis sėkmingai atstatytas';
        const successModal = <Modal show={this.state.showModal} hide={this.modalHandler}>
            <SuccessModal hide={this.modalHandler} modalMessage={modalHeader}/></Modal>;

        return (
            <Aux>
                {userCard}
                {userDetails}
                {addAccount}
                {successModal}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userRedux: state.fetch.userList,
        tokenRedux: state.auth.accessToken,
        loadingRedux: state.auth.loading,
        errorRedux: state.auth.error,
    }
};

export default connect(mapStateToProps)(AdminUserDetails);
