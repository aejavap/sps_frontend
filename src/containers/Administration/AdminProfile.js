import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import {connect} from "react-redux";
import UserCard from '../../components/DetailCards/UserCard/UserCard';
import usericon from '../../assets/usericon-guy.png';
import Aux from "../../hoc/Aux";
import AccountChange from "./UserAccount/AccountChange";
import Spinner from "../../components/UI/Spinner/Spinner";

class AdminProfile extends Component {

    state = {
        user: {
            image: usericon,
            firstName: '',
            lastName: '',
            pid: ''
        },
        showPasswordForm: false,
        showUsernameForm: false,
        error: false
    };

    componentDidMount() {
        if (this.props.tokenRedux && this.props.userDetailsRedux) {
            const authUser = this.props.userDetailsRedux;
            const updatedUser = {...authUser, image: usericon, loading: false};
            this.setState({user: updatedUser})
        }
    }

    changePasswordHandler = () => this.setState({
        showPasswordForm: !this.state.showPasswordForm,
        showUsernameForm: false
    });
    changeUsernameHandler = () => this.setState({
        showUsernameForm: !this.state.showUsernameForm,
        showPasswordForm: false
    });

    render() {
        const buttonStyle = {width: '150px'};
        let changePasswordForm = null;
        if (this.state.showPasswordForm) {
            changePasswordForm = <AccountChange back={this.changePasswordHandler} typePassword/>
        } else if (this.state.showUsernameForm) {
            changePasswordForm = <AccountChange back={this.changeUsernameHandler}/>
        }
        let userCard = null;
        if (this.state.loading) {
            userCard = <Spinner/>
        } else if (this.state.error) {
            userCard = <h2>Error</h2>
        } else {
            userCard = <UserCard
                image={this.state.user.image}
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                pid={this.state.user.pid}>
                <p>
                    <Button id='adminChangePasswordButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changePasswordHandler}>
                        Keisti slaptažodį
                    </Button>
                </p>
                <p>
                    <Button id='adminChangeUsernameButton' style={buttonStyle} bsStyle="info"
                            onClick={this.changeUsernameHandler}>
                        Keisti vartotojo vardą
                    </Button>
                </p>
            </UserCard>
        }

        return (
            <Aux>
                {userCard}
                {changePasswordForm}
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        userDetailsRedux: state.auth.authUser,
        tokenRedux: state.auth.accessToken,
    }
};

export default connect(mapStateToProps)(AdminProfile);
