import React, {Component} from 'react';
import DoctorForm from '../../../components/Form/Forms/DoctorForm';
import SpecializationForm from '../../../components/Form/Forms/SpecializationForm'
import {connect} from "react-redux";
import * as actions from '../../../store/actions/dispatchActions';
import {autofill} from "redux-form";
import Modal from "../../../components/UI/Modal/Modal";
import CreatedCard from "../../../components/UI/Modal/CreatedUser/CreatedUser";
import icon from '../../../assets/usericon.png';

class DoctorCreation extends Component {

    state = {
        isCreated: false,
        image: icon,
        error: false,
    };

    componentWillMount() {
        this.props.onFetchUserInit();
        this.props.onInitCreation();
        const url = 'api/specializations/search/by-title';
        const token = this.props.tokenRedux;
        this.props.onFetchSpecializations(url, token, 3, 'A');
    }

    componentDidMount() {
        const token = this.props.tokenRedux;
        this.props.onInitCreation();
        const number = this.props.match.params.id;
        if (typeof number !== 'undefined') {
            const url = `api/admin/doctor/${number}`;
            this.props.onFetchUser(url, token);
        }
    }

    doctorCreationHandler = values => {
        this.setState({isCreated: true});
        let url = 'api/admin/new/doctor/';
        const token = this.props.tokenRedux;
        const number = this.props.match.params.id;
        const formName = 'DoctorForm';
        const type = 'Gydytojas';
        const data = {...values, user: 'doctor', specializationId: null};
        const accountData = {username: values.username, password: values.password};
        if (typeof number !== 'undefined') {
            url = `api/admin/update/doctor/${number}`;
            this.props.onUpdateUser(url, data, token, type);
        } else {
            this.props.onCreateUser(url, data, token, formName, type, accountData)
        }
    };

    hideModal = () => {
        this.setState({isCreated: false});
        this.props.afterSubmit();
    };

    editCreatedUser = () => {
        this.props.history.replace(`/admin/register/doctor/${this.props.createdUserDataRedux.id}`);
        this.setState({isCreated: false});
    };

    specializationCreationHandler = values => {
        this.setState({isCreated: false});
        const url = 'api/admin/new/simple/specialization';
        const token = this.props.tokenRedux;
        const data = {...values, entityType: 'specialization'};
        this.props.onCreateSpecialization(url, data, token);
        this.props.afterSubmit();
    };

    filterHandler = (event, values) => {
        const token = this.props.tokenRedux;
        const url = 'api/specializations/search/by-title';
        const pageSize = '3';
        if (values.length > 1) {
            this.props.onFetchSpecializations(url, token, pageSize, values);
        } else if (values.length === 0) {
            this.props.onFetchSpecializations(url, token, pageSize, values);
        }
    };

    render() {

        let createSpecialization = null;
        let errorSpec = null;
        let showPassword = false;
        if(this.props.errorSpecRedux){
            errorSpec = <span>{this.props.errorSpecRedux}</span>
        }
        if (typeof this.props.formValues.DoctorForm !== 'undefined') {
            if (typeof this.props.formValues.DoctorForm.values !== 'undefined') {
                const values = this.props.formValues.DoctorForm.values;
                if (typeof values.specializationTitle !== 'undefined') {
                    if (values.specializationTitle === 'Kita') {
                        createSpecialization = <Modal show={values.specializationTitle === 'Kita'}>
                            <SpecializationForm onSubmit={this.specializationCreationHandler}
                                                back={this.hideModal}>
                                {errorSpec}
                            </SpecializationForm>
                        </Modal>
                    }
                }
                if (values.showPassword) {
                    showPassword = true
                }
            }
        }

        const create = typeof this.props.match.params.id === 'undefined';

        let localError = null;
        if (this.state.error) {
            localError = <h2>Tinklo klaida</h2>
        }
        let errorMessage = null;
        if (this.props.errorRedux) {
            errorMessage = <span>{this.props.errorRedux}</span>
        }
        let createdUser = null;
        let form = null;
        let specializations = null;
        let specializationTitles = null;
        if (this.props.specializationsListRedux) {
            specializations = this.props.specializationsListRedux;
            specializationTitles = specializations.map(specialization => specialization.title)
        }

        if (this.props.tokenRedux) {
            form = <DoctorForm
                specializationChange={this.filterHandler}
                spec={specializationTitles}
                created={create}
                showButtons={!createSpecialization}
                onSubmit={this.doctorCreationHandler}
                back={this.props.history.goBack}
                showPassword={showPassword}>
                {errorMessage}
            </DoctorForm>;
            if (this.props.createdUserDataRedux && this.props.createdUserDataRedux.specializationTitle && this.props.createdRedux) {
                const doctorSpec = this.props.createdUserDataRedux.specializationTitle;
                createdUser = <Modal
                    show={this.state.isCreated}
                    hide={this.hideModal}>
                    <CreatedCard
                        image={this.state.image}
                        firstName={this.props.createdUserDataRedux.firstName}
                        lastName={this.props.createdUserDataRedux.lastName}
                        pid={this.props.createdUserDataRedux.pid}
                        hide={this.hideModal}
                        back={() => this.props.history.push('/admin/home')}
                        edit={this.editCreatedUser}
                        created={create}>
                        <span>Specializacija: <strong>{doctorSpec}</strong></span>
                    </CreatedCard>
                </Modal>
            }
        }

        return (
            <div>
                {createdUser}
                {form}
                {createSpecialization}
                {localError}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        createdRedux: state.create.created,
        createdUserDataRedux: state.create.userData,
        errorRedux: state.create.error,
        tokenRedux: state.auth.accessToken,
        formValues: state.form,
        errorSpecRedux: state.specialization.errorSpec,
        specializationsListRedux: state.fetchOther.otherList
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (url, userData, token, form, type, accountData) => dispatch(actions.userCreation(url, userData, token, form, type, accountData)),
        onUpdateUser: (url, userData, token, type) => dispatch(actions.userUpdate(url, userData, token, type)),
        onInitCreation: () => dispatch(actions.creationInit()),
        onFetchUser: (url, token) => dispatch(actions.fetchUser(url, token)),
        onFetchUserInit: () => dispatch(actions.fetchUserInit()),
        afterSubmit: () => dispatch(autofill('DoctorForm', 'specializationTitle')),
        onCreateSpecialization: (url, userData, token) => dispatch(actions.specializationCreation(url, userData, token)),
        onFetchSpecializations: (url, token, pageSize, searchParams) => dispatch(actions.fetchOthers(url, token, pageSize, searchParams))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorCreation);
