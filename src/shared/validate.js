/* eslint-disable */
import {pidToDateConverter, personalIdLastDigitVerification} from './utility';

export default function (values) {
    const errors = {};
    const requiredFields = [
        'activeIngredient',
        'activeIngredientTitle',
        'activeIngredientPerDose',
        'activeIngredientUnits',
        'appointmentLength',
        'companyName',
        'companyType',
        'date',
        'diagnosisTitle',
        'dob',
        'doctorPid',
        'email',
        'firstName',
        'newPassword',
        'newPasswordConfirmation',
        'lastName',
        'password',
        'patientPid',
        'pid',
        'specializationTitle',
        'title',
        'username'
    ];
    requiredFields.forEach(field => {
        if (!values[field] || /^\s+$/.test(values[field])) {
            errors[field] = 'Privalomas laukas';
        }
    });
    const maxLength255 = [
        'email',
        'firstName',
        'lastName',
        'newPassword',
        'newPasswordConfirmation',
        'companyName',
        'password',
        'title',
        'username'
    ];
    maxLength255.forEach(field => {
        if (values[field] && values[field].length > 255) {
            errors[field] = 'Laukas negali būti ilgesnis nei 255 simboliai';
        }
    });

    const stringFields = [
        'firstName',
        'lastName'
    ];
    stringFields.forEach(field => {
        if (values[field] && !/^[a-ząčęėįšųūž„“._\- ]+$/i.test(values[field])) {
            errors[field] = 'Tik raidės, tarpai ir brūkšneliai';
        }
    });

    const numberFields = [
        'appointmentLength',
        'doctorPid',
        'patientPid',
        'pid'
    ];
    numberFields.forEach(field => {
        if (values[field] && !/^\d+$/.test(values[field])) {
            errors[field] = 'Tik skaičiai';
        }
    });
    //Will be added later
    // const minLengthFields = [
    //     'username',
    //     'password',
    //     'newPassword',
    // ];
    // minLengthFields.forEach(field => {
    //     if (values[field] && values[field].length < 8) {
    //         errors[field] = 'Mažiausiai 8 simboliai';
    //     }
    // });
    if (values.newPassword !== values.newPasswordConfirmation) {
        errors.newPasswordConfirmation = 'Slaptažodžiai nesutampa';
    }
    // Will be added later
    // if (values.password === values.username) {
    //     errors.password = 'Vartotojo vardas ir slaptažodis negali sutapti';
    // }
    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }
    const maxLength8000 = [
        'description',
        'dosageNotes',
        'notes',
    ];
    maxLength8000.forEach(field => {
        if (values[field] && values[field].length > 8000) {
            errors[field] = 'Laukas negali būti ilgesnis nei 8000 simboliai';
        }
    });
    const dateToString = new Date().toLocaleDateString("lt-LT");
    const dobToString = new Date(values.dob).toLocaleDateString("lt-LT");
    const pidFields = [
        'doctorPid',
        'doctorPidNullable',
        'patientPid',
        'pid'
    ];
    pidFields.forEach(field => {
        if (values[field] && values[field].toString().length !== 11) {
            errors[field] = 'Asmens kodas turi būti sudarytas iš 11 skaitmenų'
        }
        if (values[field] && values[field].length === 11) {
            const pidToDob = pidToDateConverter(values[field]);
            const lastDigit = personalIdLastDigitVerification(values[field]);
            const pidLast = values[field][10];
            if ((pidLast !== lastDigit) ||
                !/^(((18|19|20|21)[0-9]{2}[\-.](0[13578]|1[02])[\-.](0[1-9]|[12][0-9]|3[01]))|(18|19|20)[0-9]{2}[\-.](0[469]|11)[\-.](0[1-9]|[12][0-9]|30)|(18|19|20)[0-9]{2}[\-.](02)[\-.](0[1-9]|1[0-9]|2[0-8])|(((18|19|20)(04|08|[2468][048]|[13579][26]))|2000)[\-.](02)[\-.]29)$/.test(pidToDob)) {
                errors[field] = 'Neteisingas asmens kodo formatas';
            } else if (pidToDob > dateToString) {
                errors[field] = 'Asmuo dar nėra gimęs';
            }
        }
    });
    if (values.pid && values.dob && values.pid.length === 11) {
        const pidToDob = pidToDateConverter(values.pid);
        if (dobToString !== pidToDob) {
            errors.dob = 'Neteisinga gimimo data, nesutampa su asmens kodu';
        }
    }
    if (values.dob && dobToString > dateToString) {
        errors.dob = 'Asmuo dar nėra gimęs'
    }
    const datePattern = /^(((18|19|20)[0-9]{2}[\-.](0[13578]|1[02])[\-.](0[1-9]|[12][0-9]|3[01]))|(18|19|20)[0-9]{2}[\-.](0[469]|11)[\-.](0[1-9]|[12][0-9]|30)|(18|19|20)[0-9]{2}[\-.](02)[\-.](0[1-9]|1[0-9]|2[0-8])|(((18|19|20)(04|08|[2468][048]|[13579][26]))|2000)[\-.](02)[\-.]29)$/;
    if (values.dob && !datePattern.test(values.dob)) {
        errors.dob = 'Neteisingas datos formatas'
    }
    if (!values.unlimitedValidity && !values.validUntil) {
        errors.validUntil = 'Privaloma nurodyti galiojimo terminą arba pažymėti, kad neterminuotas'
    }
    if (values.activeIngredientPerDose && !/^\d+(\.\d+)*$/.test(values.activeIngredientPerDose)) {
        errors.activeIngredientPerDose = 'Turi būti įvestas teigiamas skaičius'
    }
    if (values.activeIngredientPerDose && Number(values.activeIngredientPerDose) === 0) {
        errors.activeIngredientPerDose = 'Negali būti 0'
    }
    if (values.diagnosisTitle && !/^[A-Z][\d]{2}([.][\d]{1,2})?/.test(values.diagnosisTitle)) {
        errors.diagnosisTitle = 'Neteisingas diagnozės kodo formatas'
    }
//////////////////////////////////////////////////////////////////////////////////////
    return errors;
}
