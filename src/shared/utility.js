/* eslint-disable */
import jwtDecode from "jwt-decode";

export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    }
};

export const pidToDateConverter = values => {
    let pidFirst = values[0];
    let dobCentury = null;
    const dobYear = values.slice(1, 3);
    const dobMonth = values.slice(3, 5);
    const dobDay = values.slice(5, 7);

    if (pidFirst === '1' || pidFirst === '2') {
        dobCentury = '18'
    } else if (pidFirst === '3' || pidFirst === '4') {
        dobCentury = '19'
    } else if (pidFirst === '5' || pidFirst === '6') {
        dobCentury = '20'
    } else if (pidFirst === '7' || pidFirst === '8') {
        dobCentury = '21'
    }
    return new Date(dobCentury + dobYear + '-' + dobMonth + '-' + dobDay).toLocaleDateString("lt-LT");
};

export const personalIdLastDigitVerification = values => {
    let lastDigit = null;
    let sum = 0;
    for (let i = 0; i < 10; i++) {
        if (i < 9) {
            sum += (parseInt(values[i], 10) * (i + 1));
        } else {
            sum += (parseInt(values[i], 10) * (1));
        }
    }
    if (sum % 11 !== 10) {
        lastDigit = (sum % 11).toString();
    } else {
        sum = 0;
        for (let i = 0; i < 10; i++) {
            if (i < 7) {
                sum += (parseInt(values[i], 10) * (i + 3));
            } else {
                sum += (parseInt(values[i], 10) * (i - 6));
            }
        }
        if (sum % 11 !== 10) {
            lastDigit = (sum % 11).toString();
        } else {
            lastDigit = '0';
        }
    }
    return lastDigit;
};

export const userType = type => {
    if (type.includes('ROLE_ADMIN')) {
        return 'admin';
    } else if (type.includes('ROLE_DOCTOR')) {
        return 'doctor';
    } else if (type.includes('ROLE_PATIENT')) {
        return 'patient';
    } else if (type.includes('ROLE_PHARMACIST')) {
        return 'pharmacist';
    }
};

export const loggedUser = type => {
    switch (type) {
        case 'admin': return 'Administratorius';
        case 'doctor': return'Gydytojas';
        case 'patient': return'Pacientas';
        case 'pharmacist': return 'Vaistininkas';
    }
};

export const role = token => {
    const decodedToken = jwtDecode(token);
    const roles = decodedToken.authorities;
    return userType(roles);
};

export const toFirstUpperCaseToLowerCase = value => value && value.replace(/[^a-ząčęėįšųūž„“._\- ]/i, '').charAt(0).toUpperCase() + value.replace(/[^a-ząčęėįšųūž„“._\- ]/i, '').slice(1).toLowerCase();
export const onlyNumbers = value => value && value.replace(/[^\d]/g, '');
export const onlyFloatNumbers = value => value && value.replace(',', '.').replace(/[^\d\.]/g, "").replace(/\./, "x").replace(/\./g, "").replace(/x/, ".");
export const toUpperCase = value => value && value.replace(/[^a-ząčęėįšųūž„“._\- ]/i, '').toUpperCase();

export const stringToDate = value => {
    if (!value) {
        return value
    }
    const onlyNums = value.replace(/[^\d]/g, '');
    if (onlyNums.length <= 4) {
        return onlyNums
    }
    if (onlyNums.length <= 6) {
        return `${onlyNums.slice(0, 4)}-${onlyNums.slice(4)}`
    }
    return `${onlyNums.slice(0, 4)}-${onlyNums.slice(4, 6)}-${onlyNums.slice(6, 8)}`
};

export const userProperty = value => {
    switch (value) {
        case 'companyName' : return 'Įmonės pavadinimas';
        case 'companyType' : return 'Įmonės teisinė forma';
        case 'createdOn' : return 'Registracijos data';
        case 'dob' : return 'Gimimo data';
        case 'doctorId' : return 'Gydytojo identifikacinis numeris';
        case 'doctorPid' : return 'Gydytojo asmens kodas';
        case 'doctorPidNullable' : return 'Gydytojo asmens kodas';
        case 'enabled' : return 'Aktyvi paskyra?';
        case 'firstName' : return 'Vardas';
        case 'hasAccount' : return 'Yra paskyra?';
        case 'id' : return 'Identifikacinis numeris';
        case 'lastName' : return 'Pavardė';
        case 'pid' : return 'Asmens kodas';
        case 'roles' : return 'Vartotojo tipas';
        case 'specializationId' : return 'Specializacijos kodas';
        case 'specializationTitle' : return 'Specializacija';
        case 'user' : return 'Vartotojo tipas';
        case 'username' : return 'Vartotojo vardas';
        default : return 'Kita informacija';
    }
};
