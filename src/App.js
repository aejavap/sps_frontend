import React, { Component } from "react";
import {withRouter} from "react-router-dom";
import "./App.css";
import Routes from "./Routes";
import Navbar from "./components/Navigation/Navbar/Navbar";
import {connect} from "react-redux";
import * as actions from './store/actions/authentication';
import {loggedUser} from './shared/utility';

class App extends Component {

    componentWillMount() {
        this.props.onTryAutoLogin()
    }

    render() {
        let navbar = <Navbar isAuthenticated={this.props.isAuthenticatedRedux}/>;

        if(this.props.authUserRedux){
            navbar = <Navbar
                isAuthenticated={this.props.isAuthenticatedRedux}
                userType={loggedUser(this.props.authUserRedux.user)}
                firstName={this.props.authUserRedux.firstName}
                lastName={this.props.authUserRedux.lastName}
                
            />
        }
        return (
            <div>
                {navbar}
                <Routes />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticatedRedux: state.auth.accessToken,
        authUserRedux: state.auth.authUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoLogin: () => dispatch(actions.authCheckState())
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
